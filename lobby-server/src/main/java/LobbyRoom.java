package mafia.lobby;

import mafia.User;
import java.util.*;
import org.json.*;

class LobbyRoom {
    private final int roomId;
    private User host;
    private Set<User> users;
    private List<String> roles;

    LobbyRoom(int roomId) {
	users = new HashSet<User>();
	this.roomId = roomId;
	roles = new ArrayList<String>();
    }

    LobbyRoom(int roomId, User host) {
	this(roomId);
	try {
	    addUser(host);
	    this.host = host;
	} catch(IllegalArgumentException e) {
	    System.out.println("IllegalArgumentException: " + e.getMessage());
	    e.printStackTrace();
	}
    }
    
    int getRoomId() {
	return roomId;
    }

    User getHost() {
	return host;
    }

    Set<User> getUsers() {
	return users;
    }

    List<String> getRoles() {
	return roles;
    }

    void setHost(User host) throws IllegalArgumentException {
	for(User currentUser : users) {
	    if(currentUser.getName().equalsIgnoreCase(host.getName())) {
		this.host = host;
		return;
	    }
	}
	throw new IllegalArgumentException("User is not in room.");
    }

    void addUser(User user) throws IllegalArgumentException {
	for(User currentUser : users) {
	    if(currentUser.getName().equalsIgnoreCase(user.getName())) {
		throw new IllegalArgumentException("User is already in room.");
	    }
	}
	users.add(user);
    }
    
    void removeUserById(String userId) throws NoSuchElementException {
	for(User targetUser : users) {
	    if(userId.equals(targetUser.getId())) {
		users.remove(targetUser);
		return;
	    }
	}
	throw new NoSuchElementException("User is not in room.");
    }
  
    void removeUserByName(String userName) throws NoSuchElementException {
	for(User targetUser : users) {
	    if(userName.equalsIgnoreCase(targetUser.getName())) {
		users.remove(targetUser);
		return;
	    }
	}
	throw new NoSuchElementException("User is not in room.");
    }

    void removeUser(User user) throws NoSuchElementException {
	removeUserByName(user.getName());
    }

    void addRole(String role) {
	roles.add(role);
    }

    void removeRole(String role) {
	roles.remove(role);
    }

    String toJSON() {
	JSONObject jo = new JSONObject();
	jo.put("roomId", roomId);
	jo.put("hostName", host.getName());
	return jo.toString();

	/*
	StringBuilder builder = new StringBuilder();
	return builder.
	    append("{\"roomId\":").
	    append(roomId).
	    append(",\"hostName\":\"").
	    append(host.getName()).
	    append("\"}").toString(); 
	*/
    }
}
