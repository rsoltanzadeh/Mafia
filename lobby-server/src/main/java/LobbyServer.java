package mafia.lobby;

import mafia.User;
import java.sql.*;
import javax.sql.*;
import java.io.IOException;
import java.util.*;
import javax.websocket.*;
import javax.websocket.server.*;
import org.json.*;

/**
 * Handles lobby messages and game configurations by the lobby host. Sends a randomly generated game ID-token to all players in the lobby once the game is ready to start that will be used by GameServer.
 *
 * @author Ramin Soltanzadeh
 * @version 1.0
 */ 
public class LobbyServer extends Endpoint {
    private Session session;
    private ServerEndpointConfig endpointConfig;
    private LobbyConfigurator lobbyConfig;
    private String userName;
    private boolean authenticated;
    private DataSource dataSource;

    @Override
    public void onOpen(Session session, EndpointConfig endpointConfig) {
	this.session = session;
	this.endpointConfig = (ServerEndpointConfig) endpointConfig;
	lobbyConfig = (LobbyConfigurator) this.endpointConfig.getConfigurator();
	this.authenticated = false;
	try {
	    dataSource = (DataSource) new javax.naming.InitialContext().lookup("java:/MySqlDS");
	} catch(javax.naming.NamingException e) {
	    //System.out.println("NamingException: " + e.getMessage());
	    //e.printStackTrace();
	}
	this.session.addMessageHandler(new MessageHandler.Whole<String>() {
		LobbyConfigurator lobbyConfig = LobbyServer.this.lobbyConfig;
		Session session = LobbyServer.this.session;
		
		@Override
		public void onMessage(String text) {
		    if(!LobbyServer.this.authenticated && !text.startsWith(NormalMessage.AUTHENTICATOR.getValue())) {
			System.out.println("No authenticator provided. Closing connection.");
			try {
			    session.close(new CloseReason(new CloseReason.CloseCode() {
				    @Override
				    public int getCode() {
					return 1008;
				    }
				}, "No authenticator provided."));
			} catch(IOException e) {
			    System.out.println("IOException: " + e.getMessage());
			    e.printStackTrace();
			}
		    } else if(text.startsWith(NormalMessage.MESSAGE.getValue())) {
			for(Session otherSession : session.getOpenSessions()) {
			    try {
				System.out.println(session.getId() + " (" + LobbyServer.this.userName + ") sending message to " + otherSession.getId());
				otherSession.getBasicRemote().sendText(NormalResponse.MESSAGE.getValue() + ":" + LobbyServer.this.userName + ":" + text.substring(NormalMessage.MESSAGE.getValue().length() +1));
			    } catch(IOException e) {
				System.out.println("Error sending message: " + e.getMessage());
			    } //replace below code with above code for debug purposes
			    /*LobbyRoom myRoom = lobbyConfig.getRoomByUserId(session.getId());
			    if(myRoom != null && lobbyConfig.isInRoomById(myRoom.getRoomId(), otherSession.getId())) {
				try {
				    otherSession.getBasicRemote().sendText(NormalResponse.MESSAGE.getValue() + LobbyServer.this.userName + ":" + text.substring(NormalMessage.MESSAGE.getValue().length()));
				} catch (IOException e) {
				    System.out.println("Error sending message: " + e.getMessage());
				}
				}*/
			}
		    } else if(text.startsWith(NormalMessage.ADD_ROLE.getValue())) {
			String roleName = text.substring(NormalMessage.ADD_ROLE.getValue().length());
			LobbyRoom myRoom = lobbyConfig.getRoomByUserId(session.getId());
			if(!myRoom.getHost().getId().equals(session.getId())) {
			    System.out.println("Non-host trying to add role. Returning.");
			    return;
			}
			myRoom.addRole(roleName);
			Set<User> users = myRoom.getUsers();
			outerloop: for(User user : users) {
			    for(Session otherSession : session.getOpenSessions()) {
				if(user.getId().equals(otherSession.getId())) {
				    try {
					otherSession.getBasicRemote().sendText(NormalResponse.CHOSEN_ROLES.getValue() + getChosenRoles(myRoom));
				    } catch(IOException e) {
					System.out.println("IOException: " + e.getMessage());
					e.printStackTrace();
				    }
				    continue outerloop;
				}
			    }
			}
		    } else if(text.startsWith(NormalMessage.REMOVE_ROLE.getValue())) {
			String roleName = text.substring(NormalMessage.REMOVE_ROLE.getValue().length());
			LobbyRoom myRoom = lobbyConfig.getRoomByUserId(session.getId());
			if(!myRoom.getHost().getId().equals(session.getId())) {
			    System.out.println("Non-host trying to remove role. Returning.");
			    return;
			}
			myRoom.removeRole(roleName);
			Set<User> users = myRoom.getUsers();
			outerloop: for(User user : users) {
			    for(Session otherSession : session.getOpenSessions()) {
				if(user.getId().equals(otherSession.getId())) {
				    try {
					otherSession.getBasicRemote().sendText(NormalResponse.CHOSEN_ROLES.getValue() + getChosenRoles(myRoom));
				    } catch(IOException e) {
					System.out.println("IOException: " + e.getMessage());
					e.printStackTrace();
				    }
				    continue outerloop;
				}
			    }
			}

		    } else if(text.startsWith(NormalMessage.START_GAME.getValue())) {
			LobbyRoom thisRoom = lobbyConfig.getRoomByUserName(userName);
			if(!thisRoom.getHost().getName().equals(userName)) {
			    System.out.println("Non-host trying to start game. Returning.");
			    return;
			} else if(thisRoom.getUsers().size() != thisRoom.getRoles().size()) {
			    System.out.println("Wrong amount of users/roles. " + thisRoom.getUsers().size() + " users and " + thisRoom.getRoles().size() + " roles.");
			} else {
			    String chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
			    int tokenLength = 32; // entropy == Math.pow(chars.length(), tokenLength)
			    Random random = new Random();
			    StringBuilder token = new StringBuilder(tokenLength);
			    for(int i = 0; i < tokenLength; i++) {
				token.append(chars.charAt(random.nextInt(tokenLength)));
			    }
			    int seed = thisRoom.getUsers().size();
			    List<String> roles = thisRoom.getRoles();
			    outerloop: for(User currentUser : thisRoom.getUsers()) {
				for(Session otherSession : session.getOpenSessions()) {
				    if(currentUser.getId().equals(otherSession.getId())) {
					int randomNumber = random.nextInt(seed--);
					String role = roles.get(randomNumber);
					roles.remove(randomNumber);
					try {
					    otherSession.getBasicRemote().sendText(NormalResponse.GAME_TOKEN.getValue() + token.toString() + ":" + role);
					} catch(IOException e) {
					    System.out.println("IOException: " + e.getMessage());
					    e.printStackTrace();
					}
				    }
				}
			    }
			}
		    } else if(text.startsWith(NormalMessage.JOIN_ROOM.getValue())) {
			int targetRoomId = Integer.parseInt(text.substring(NormalMessage.JOIN_ROOM.getValue().length()));
			LobbyRoom targetRoom = lobbyConfig.getRoomById(targetRoomId);
			leaveRoomIfExists();
			try {
			    targetRoom.addUser(new User(session.getId(), userName));
			    session.getBasicRemote().sendText(NormalResponse.JOINED_ROOM.getValue() + targetRoomId);
			    session.getBasicRemote().sendText(NormalResponse.HOST.getValue() + targetRoom.getHost().getName());
			    session.getBasicRemote().sendText(NormalResponse.CHOSEN_ROLES.getValue() + getChosenRoles(targetRoom));
			    for(User user : targetRoom.getUsers()) {
				for(Session otherSession : session.getOpenSessions()) {
				    if(otherSession.getId().equals(user.getId())) {
					otherSession.getBasicRemote().sendText(NormalResponse.USER_JOINED_ROOM.getValue() + userName);
				    }
				}
			    }
			} catch(IOException e) {
			    System.out.println("IOException: " + e.getMessage());
			    e.printStackTrace();
			}
		    } else if(text.startsWith(NormalMessage.GET_ROOMS.getValue())) {
			System.out.println("Getting rooms.");
			Set<LobbyRoom> rooms = lobbyConfig.getRooms();
			StringBuilder builder = new StringBuilder();
			builder.append("[");
			String prefix = "";
			for(LobbyRoom room : rooms) {
			    builder.append(prefix);
			    prefix = ",";
			    builder.append(room.toJSON());
			}
			builder.append("]");

			try {
			    session.getBasicRemote().sendText(NormalResponse.ROOMS.getValue() + builder.toString());
			} catch(IOException e) {
			    System.out.println("IOException: " + e.getMessage());
			    e.printStackTrace();
			}
			
		    } else if(text.startsWith(NormalMessage.LEAVE_ROOM.getValue())) {
			leaveRoomIfExists();
			try {
			    session.getBasicRemote().sendText(NormalResponse.LEFT_ROOM.getValue());
			} catch(IOException e) {
			    System.out.println("IOException: " + e.getMessage());
			    e.printStackTrace();
			}
		    } else if(text.startsWith(NormalMessage.CREATE_ROOM.getValue())) {
			leaveRoomIfExists();
			LobbyRoom newRoom = lobbyConfig.createRoom(new User(session.getId(), userName));
			System.out.println(userName + " created room " + newRoom.getRoomId());
			try {
			    session.getBasicRemote().sendText(NormalResponse.JOINED_ROOM.getValue());
			    session.getBasicRemote().sendText(NormalResponse.HOST.getValue() + userName);
			} catch(IOException e) {
			    System.out.println("IOException: " + e.getMessage());
			    e.printStackTrace();
			}
		    } else if(text.startsWith(NormalMessage.AUTHENTICATOR.getValue())) {
			String name = text.substring(NormalMessage.AUTHENTICATOR.getValue().length() + 1);
			System.out.println("Authenticating '" + name + "'.");
			String pass = text.substring(text.indexOf(':') + 1);
			LobbyServer.this.userName = name; // to be removed in production
			LobbyServer.this.authenticated = true; // to be removed in production
			LobbyServer.this.lobbyConfig.addUser(session.getId(), name);
			for(Session otherSession : session.getOpenSessions()) {
			    try {
				otherSession.getBasicRemote().sendText(NormalResponse.USER_JOINED_ROOM.getValue() + ":" + LobbyServer.this.userName);
			    } catch(IOException e) {
				System.out.println("Error sending message: " + e.getMessage());
			    }
			} // to be removed in production
			try {
			    List<String> onlineUsers = new ArrayList<String>();
			    for(String uname : LobbyServer.this.lobbyConfig.getMap().values()) {
				onlineUsers.add(uname);
			    }
			    LobbyServer.this.session.getBasicRemote().sendText(NormalResponse.AUTHENTICATED.getValue() + ":" + Arrays.toString(onlineUsers.toArray())); // to be removed in production
			    
			} catch(IOException e) {
			    System.out.println("Error sending message: " + e.getMessage());
			}
			return; // to be removed in production
			/* try(Connection conn = dataSource.getConnection("ramin", "password");			    
			    Statement stmt = conn.createStatement();
			    ResultSet rs = stmt.executeQuery("SELECT password FROM mafia.users WHERE username = '" + name + "';")) {
			    rs.next();
			    if(rs.getString("password").equals(pass)) {
				LobbyServer.this.userName = name;
				session.getBasicRemote().sendText(NormalResponse.AUTHENTICATED.getValue());
				LobbyServer.this.authenticated = true;
			    } else {
				session.close(new CloseReason(new CloseReason.CloseCode() {
					@Override
					public int getCode() {
					    return 1000;
					}
				    }, "Invalid authenticator."));
			    }
			} catch(SQLException e) {
			    System.out.println("SQLException: " + e.getMessage());
			    e.printStackTrace();
			} catch(IOException e) {
			    System.out.println("IOException: " + e.getMessage());
			    e.printStackTrace();
			    } */ 
		    } else {
			System.out.println(text);
		    }
		}
	    });
    }

    @Override
    public void onClose(Session session, CloseReason closeReason) {
	LobbyServer.this.lobbyConfig.removeUser(session.getId());
	for(Session otherSession : session.getOpenSessions()) {
			    try {
				if(otherSession.getId().equals(session.getId())) {
				    continue;
				}
				otherSession.getBasicRemote().sendText(NormalResponse.USER_LEFT_ROOM.getValue() + ":" + LobbyServer.this.userName);
			    } catch(IOException e) {
				System.out.println("Error sending message: " + e.getMessage());
			    }

	} // to be removed in production
	leaveRoomIfExists();
    }
    
    public void leaveRoomIfExists() {
	String sessionId = session.getId();
	LobbyRoom currentRoom = lobbyConfig.getRoomByUserId(sessionId);
	if(currentRoom == null) {
	    return;
	}
	int roomId = currentRoom.getRoomId();
	boolean isHost = currentRoom.getHost().getId().equals(sessionId);
	if(isHost) {
	    Set<User> users = currentRoom.getUsers();
	    if(users.size() == 1) {
		lobbyConfig.deleteRoom(roomId);
		System.out.println("Deleted room " + roomId);
		return;
	    }
	    System.out.println(userName + " is leaving room as host. Randomly assigning new host to room " + roomId + ".");
	    int item = new Random().nextInt(users.size());
	    while(true) {
		System.out.println("Inside while-loop.");
		int i = 0;
		for(User user : users) {
		    if(i == item) {
			if(user.getId() == sessionId) {
			    continue;
			} else {
			    currentRoom.setHost(user);
			    System.out.println(user.getName() + " is the new host of room " + roomId + ".");
			    for(User currentUser : users) {
				String id = currentUser.getId();
				for(Session otherSession : session.getOpenSessions()) {
				    if(otherSession.getId().equals(id) && !otherSession.getId().equals(session.getId())) {
					try {
					    otherSession.getBasicRemote().sendText(NormalResponse.HOST.getValue() + user.getName());
					    return;
					} catch(IOException e) {
					    System.out.println("IOException: " + e.getMessage());
					    e.printStackTrace();
					}
				    }
				}
			    }
			}
		    }
		    i++;
		}
		return;
	    }
	}
	currentRoom.removeUserById(sessionId);
	for(User currentUser : currentRoom.getUsers()) {
	    for(Session otherSession : session.getOpenSessions()) {
		if(currentUser.getId().equals(otherSession.getId())) {
		    try {
			otherSession.getBasicRemote().sendText(NormalResponse.USER_LEFT_ROOM.getValue() + userName);
		    } catch(IOException e) {
			System.out.println("IOException: " + e.getMessage());
			e.printStackTrace();
		    }
		}
	    }
	}
	System.out.println(userName + " left room " + roomId);
    }

    public String getChosenRoles(LobbyRoom room) {
	List<String> roles = room.getRoles();
	StringBuilder builder = new StringBuilder();
	builder.append("[");
	String prefix = "";
	for(String role : roles) {
	    builder.append(prefix).
		append("\"").
		append(role).
		append("\"");
	    prefix = ",";
	}
	builder.append("]");
	return builder.toString();
    }
}

enum NormalResponse {
    JOINED_ROOM("joined-room"),
    LEFT_ROOM("left-room"),
    USER_JOINED_ROOM("user-joined-room"),
    USER_LEFT_ROOM("user-left-room"),
    ROOMS("rooms"),
    CHOSEN_ROLES("chosen-roles"),
    AUTHENTICATED("authenticated"),
    MESSAGE("message"),
    HOST("host"),
    GAME_TOKEN("game-token");
    
    private final String value;

    private NormalResponse(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    @Override
    public String toString() {
	return getValue();
    }
}

enum NormalMessage {
    GET_ROOMS("get-rooms"),
    CREATE_ROOM("create-room"),
    JOIN_ROOM("join-room"),
    LEAVE_ROOM("leave-room"),
    ADD_ROLE("add-role"),
    REMOVE_ROLE("remove-role"),
    START_GAME("start-game"),
    MESSAGE("message"),
    AUTHENTICATOR("authenticator");
    private final String value;

    private NormalMessage(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    @Override
    public String toString() {
	return getValue();
    }
}

