package mafia.lobby;

import java.util.*;
import javax.websocket.*;
import javax.websocket.server.*;

public class LobbyServerConfig implements ServerApplicationConfig {
    
    @Override
    public Set<ServerEndpointConfig> getEndpointConfigs(Set<Class<? extends Endpoint>> endpointClasses) {
	Set<ServerEndpointConfig> configs = new HashSet<ServerEndpointConfig>();
	ServerEndpointConfig sec = ServerEndpointConfig.Builder.create(LobbyServer.class, "/lobby").configurator(new LobbyConfigurator()).build();
	configs.add(sec);
	return configs;
    }

    public Set<Class<?>> getAnnotatedEndpointClasses(Set<Class<?>> scanned) {
	return scanned;
    }
}
