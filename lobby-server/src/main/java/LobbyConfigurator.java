package mafia.lobby;

import mafia.User;
import java.util.*;
import javax.websocket.*;
import javax.websocket.server.*;

/**
 * This is the Configurator for the LobbyServer class. All LobbyServer instances share the same instance of this class. Used for global server state.
 *
 * @author Ramin Soltanzadeh
 * @version 1.0
 */
class LobbyConfigurator extends ServerEndpointConfig.Configurator {
    private static int lastRoomId = 0;
    private Set<LobbyRoom> rooms;
    private Map<String,String> userIdMap; //remove in prod
    LobbyConfigurator() {
	rooms = new HashSet<LobbyRoom>();
	userIdMap = new HashMap<String,String>(); //remove in prod
    }

    //remove in prod
    void addUser(String userId, String username) {
	userIdMap.put(userId, username);
    }

    String getUsername(String userId) {
	return userIdMap.get(userId);
    }

    void removeUser(String userId) {
	userIdMap.remove(userId);
    }

    Map<String,String> getMap() {
	return userIdMap;
    }
    /**
     * creates an empty room
     */
    LobbyRoom createRoom() {
	LobbyRoom newRoom = new LobbyRoom(++lastRoomId);
	rooms.add(newRoom);
	return newRoom;
    }

    /**
     * creates a room with a specified host; throws IllegalArgumentException if host is already the host of another room
     */
    LobbyRoom createRoom(User host) throws IllegalArgumentException {
	for(LobbyRoom room : rooms) {
	    if(room.getHost().getName().equalsIgnoreCase(host.getName())) {
		throw new IllegalArgumentException("User cannot be host of two rooms simultaneously.");
	    }
	}
	LobbyRoom newRoom = new LobbyRoom(++lastRoomId, host);
	rooms.add(newRoom);
	return newRoom;
    }
    
    void deleteRoom(int roomId) {
	for(LobbyRoom currentRoom : rooms) {
	    if(currentRoom.getRoomId() == roomId) {
		rooms.remove(currentRoom);
		return;
	    }
	}
    }

    /**
     * sets the host of a room to the specified user; throws IllegalArgumentException if host is already host of another room or if room doesn't exist 
     */
    void setHost(int roomId, User host) throws IllegalArgumentException {
	LobbyRoom room = null;
	for(LobbyRoom currentRoom : rooms) {
	    if(currentRoom.getHost().getName().equalsIgnoreCase(host.getName())) {
		throw new IllegalArgumentException("User cannot be host of two rooms simultaneously.");
	    }
	    if(currentRoom.getRoomId() == roomId) {
		room = currentRoom;
	    }
	}
	if(room == null) {
	    throw new IllegalArgumentException("Room does not exist.");
	}
	room.setHost(host);
    }
    
    Set<LobbyRoom> getRooms() {
	return rooms;
    }
    
    LobbyRoom getRoomByHostId(String hostId) {
	for(LobbyRoom room : rooms) {
	    if(room.getHost().getId().equals(hostId)) {
		return room;
	    }
	}
	return null;
    }
    
    LobbyRoom getRoomByHostName(String hostName) {
	for(LobbyRoom room : rooms) {
	    if(room.getHost().getName().equalsIgnoreCase(hostName)) {
		return room;
	    }
	}
	return null;
    }
    
    LobbyRoom getRoomById(int roomId) {
	for(LobbyRoom room : rooms) {
	    if(room.getRoomId() == roomId) {
		return room;
	    }
	}
	return null;
    }
    
    LobbyRoom getRoomByUserId(String userId) {
	for(LobbyRoom room : rooms) {
	    for(User user : room.getUsers()) {
		if(user.getId().equals(userId)) {
		    return room;
		}
	    }
	}
	return null;
    }

    LobbyRoom getRoomByUserName(String userName) {
	for(LobbyRoom room : rooms) {
	    for(User user : room.getUsers()) {
		if(user.getName().equalsIgnoreCase(userName)) {
		    return room;
		}
	    }
	}
	return null;
    }

    /**
     * gets user that is the host of the specified room; throws IllegalArgumentException if room does not exist
     */
    User getHostByRoom(int roomId) throws IllegalArgumentException {
	for(LobbyRoom room : rooms) {
	    if(room.getRoomId() == roomId) {
		return room.getHost();
	    }
	}
	throw new IllegalArgumentException("Room does not exist.");
    }
    
    boolean isInRoomById(int roomId, String userId) {
	for(LobbyRoom room : rooms) {
	    if(room.getRoomId() == roomId) {
		for(User user : room.getUsers()) {
		    if(user.getId().equals(userId)) {
			return true;
		    }
		}
	    }
	}
	return false;
    }

    boolean isInRoomByName(int roomId, String userName) {
	for(LobbyRoom room : rooms) {
	    if(room.getRoomId() == roomId) {
		for(User user : room.getUsers()) {
		    if(user.getName().equalsIgnoreCase(userName)) {
			return true;
		    }
		}
	    }
	}
	return false;
    }
}
