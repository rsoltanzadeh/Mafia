package mafia;
/**
 * Represents a user.
 * @author Ramin Soltanzadeh
 * @version 0.1
 */
public class User {
    private String userId;
    private String userName;

    /**
     * @param userId UID
     * @param userName username
     */
    public User(String userId, String userName) {
	this.userId = userId;
	this.userName = userName;
    }
    /**
     * Gets the UID.
     * @returns UID
     */
    public String getId() {
	return userId;
    }
    /**
     * Gets the username.
     * @returns Username
     */
    public String getName() {
	return userName;
    }
}
