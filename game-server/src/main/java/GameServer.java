package mafia.game;

import java.sql.*;
import javax.sql.*;
import java.io.IOException;
import java.util.*;
import javax.websocket.*;
import javax.websocket.server.*;

/**
 * Turns lobbies into games and handles incoming in-game commands and messages from players. Every player is connected to their own GameServer instance.
 * 
 * @author Ramin Soltanzadeh
 * @version 1.0
 */
public class GameServer extends Endpoint {
    private Session session;
    private ServerEndpointConfig endpointConfig;
    private GameConfigurator gameConfig;
    private DataSource dataSource;
    private boolean authenticated;
    private boolean hasGivenToken;
    private boolean gameHasStarted;
    private String userName;
    private String alias;
    private String gameId;
    private Role role;
    private Game game;

    @Override
    public void onOpen(Session session, EndpointConfig endpointConfig) {
	this.session = session;
	this.endpointConfig = (ServerEndpointConfig) endpointConfig;
	gameConfig = (GameConfigurator) this.endpointConfig.getConfigurator();
	this.authenticated = false;
	this.hasGivenToken = false;
	this.gameHasStarted = false;
	try {
	    dataSource = (DataSource) new javax.naming.InitialContext().lookup("java:/MySqlDS");
	} catch(javax.naming.NamingException e) {
	    System.out.println("NamingException: " + e.getMessage());
	    e.printStackTrace();
	}
	this.session.addMessageHandler(new MessageHandler.Whole<String>() {
		GameConfigurator gameConfig = GameServer.this.gameConfig;
		Session session = GameServer.this.session;

		@Override
		public void onMessage(String text) {
		    if(!GameServer.this.gameHasStarted) {
			if(!GameServer.this.authenticated && !text.startsWith(NormalMessage.AUTHENTICATOR.getValue())) {
			    System.out.println("No authenticator provided. Closing connection.");
			    try {
				session.close(new CloseReason(new CloseReason.CloseCode() {
					@Override
					public int getCode() {
					    return 1008;
					}
				    }, "No authenticator provided."));
			    } catch(IOException e) {
				System.out.println("IOException: " + e.getMessage());
				e.printStackTrace();
			    }
                            /**
                             * testing
                             */
			} else if(text.startsWith(NormalMessage.AUTHENTICATOR.getValue())) {
			    String name = text.substring(NormalMessage.AUTHENTICATOR.getValue().length(), text.indexOf(':'));
			    System.out.println("Authenticating '" + name + "'.");
			    String pass = text.substring(text.indexOf(':') + 1);
			    try(Connection conn = dataSource.getConnection("ramin", "password");
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT password FROM mafia.users WHERE username = '" + name + "';")) {
				rs.next();
				if(rs.getString("password").equals(pass)) {
				    GameServer.this.userName = name;
				    GameServer.this.authenticated = true;
				    session.getBasicRemote().sendText(NormalResponse.AUTHENTICATED.getValue());
				} else {
				    session.close(new CloseReason(new CloseReason.CloseCode() {
					    @Override
					    public int getCode() {
						return 1000;
					    }
					}, "Invalid authenticator."));
				}
			    } catch(SQLException e) {
				System.out.println("SQLException: " + e.getMessage());
				e.printStackTrace();
			    } catch(IOException e) {
				System.out.println("IOException: " + e.getMessage());
				e.printStackTrace();
			    }
			} else if(!GameServer.this.hasGivenToken && !text.startsWith(NormalMessage.GAME_TOKEN.getValue())) {
			    System.out.println("No token provided. Closing connection.");
			    try {
				session.close(new CloseReason(new CloseReason.CloseCode() {
					@Override
					public int getCode() {
					    return 1008;
					}
				    }, "No token provided."));
			    } catch(IOException e) {
				System.out.println("IOException: " + e.getMessage());
				e.printStackTrace();
			    }
			} else if(text.startsWith(NormalMessage.GAME_TOKEN.getValue())) {
			    System.out.println("Received token: " + text);
			    GameServer.this.gameId = text.substring(NormalMessage.GAME_TOKEN.getValue().length(), text.indexOf(':'));

			    String roleName = text.substring(text.indexOf(':') + 1);
			    Set<Role> pendingRoles = gameConfig.getPendingRoles(gameId);
			    int number;
			    if(pendingRoles == null) {
				number = 1;
			    } else {
				number = pendingRoles.size() + 1;
			    }
			    switch(roleName) {
			    case "bodyguard":
				GameServer.this.role = new Bodyguard(userName, number, "");
				break;
			    case "doctor": 
				GameServer.this.role = new Doctor(userName, number, "");
				break;
			    case "godfather": 
				GameServer.this.role = new Godfather(userName, number, "");
				break;
			    case "survivor": 
				GameServer.this.role = new Survivor(userName, number, "");
				break;
			    default: 
				System.out.println("Invalid role chosen. Returning.");
				return;
			    }
			    try {
				gameConfig.linkEndpointToGame(GameServer.this, gameId);
				session.getBasicRemote().sendText(NormalResponse.TOKEN_RECEIVED.getValue());
			    } catch(IllegalArgumentException e) {
				System.out.println("IllegalArgumentException: " + e.getMessage());
				e.printStackTrace();
			    } catch(IOException e) {
				System.out.println("IOException: " + e.getMessage());
				e.printStackTrace();
			    }
			    gameConfig.addPendingRole(gameId, GameServer.this.role, session);
			    GameServer.this.hasGivenToken = true;
			} else if(text.startsWith(NormalMessage.ALIAS.getValue())) {
			    String alias = text.substring(NormalMessage.ALIAS.getValue().length());
			    if(gameConfig.setAlias(gameId, role, alias)) {
				for(Session otherSession : gameConfig.getPendingSessions(gameId)) {
				    if(otherSession.isOpen()) {
					try {
					    otherSession.getBasicRemote().sendText(NormalResponse.ALIAS.getValue() + alias);
					} catch(IOException e) {
					    System.out.println("IOException: " + e.getMessage());
					    e.printStackTrace();
					}
				    } else {
					System.out.println("Someone left the game during name selection.");
				    }
				}
			    } else {
				try {
				    session.getBasicRemote().sendText(ErrorResponse.ALIAS_TAKEN.getValue());
				} catch(IOException e) {
				    System.out.println("IOException: " + e.getMessage());
				    e.printStackTrace();
				}
			    }
			}
		    } else {
			//game has started
			if(text.startsWith(NormalMessage.TRIAL_VOTE_ADD.getValue())) {
			    int number = Integer.parseInt(text.substring(NormalMessage.TRIAL_VOTE_ADD.getValue().length()));
			    game.handleTrialVoteAdd(role, number); 
			} else if(text.startsWith(NormalMessage.TRIAL_VOTE_REMOVE.getValue())) {
			    game.handleTrialVoteRemove(role);
			} else if(text.startsWith(NormalMessage.VOTE_INNOCENT.getValue())) {
			    game.handleVoteInnocent(role);
			} else if(text.startsWith(NormalMessage.VOTE_GUILTY.getValue())) {
			    game.handleVoteGuilty(role);
			} else if(text.startsWith(NormalMessage.VOTE_REMOVE.getValue())) {
			    game.handleVoteRemove(role);
			} else if(text.startsWith(NormalMessage.TARGET.getValue())) {
                            int number = Integer.parseInt(text.substring(NormalMessage.TARGET.getValue().length()));
                            game.handleTarget(role, number);
			} else if(text.startsWith(NormalMessage.SECOND_TARGET.getValue())) {
                            int number = Integer.parseInt(text.substring(NormalMessage.SECOND_TARGET.getValue().length()));
                            game.handleSecondTarget(role, number);
			} else if(text.startsWith(NormalMessage.MESSAGE.getValue())) {
			    String message = text.substring(NormalMessage.MESSAGE.getValue().length());
			    game.handleMessage(role, message);
			}
		    }
		}
	    });
    }

    /**
     * gets a reference to the game and flags it as started
     */
    public void setGame(Game game) {
	this.game = game;
	gameHasStarted = true;
    }
}
