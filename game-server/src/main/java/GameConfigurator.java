package mafia.game;

import java.util.*;
import javax.websocket.*;
import javax.websocket.server.*;

/**
 * This is the Configurator for the GameServer class. All GameServer instances share the same instance of this class. Used for global server state. 
 *
 * @author Ramin Soltanzadeh
 * @version 1.0
 */
class GameConfigurator extends ServerEndpointConfig.Configurator {
    Set<Game> games;
    Set<PendingGame> pendingGames;

    GameConfigurator() {
	games = new HashSet<Game>();
	pendingGames = new HashSet<PendingGame>();
    }

    /**
     * sets a temporary nickname for a player to keep their identity secret during the game
     *
     * @param gameId unique identifier of the game the player is in
     * @param role object representing a player
     * @param alias the nickname to be given to the player
     */
    boolean setAlias(String gameId, Role role, String alias) {
	for(PendingGame pg : pendingGames) {
	    if(pg.getId().equals(gameId)) {
		for(Role currentRole : pg.getRoles()) {
		    if(currentRole.getAlias().equals(alias)) {
			return false;
		    }
		}
		role.setAlias(alias);
		return true;
	    }
	}
	return false;
    }

    /**
     * links a GameServer instance to a game that has not yet started
     */
    void linkEndpointToGame(GameServer endpoint, String gameId) {
	for(PendingGame pg : pendingGames) {
	    if(pg.getId().equals(gameId)) {
		pg.addEndpoint(endpoint);
		return;
	    }
	}
	PendingGame newPG = new PendingGame(gameId);
	newPG.addEndpoint(endpoint);
    }

    /**
     * adds a player to a game that has not yet started
     *
     * @param role the player
     * @param session the unique session between the player and their GameServer instance
     */
    void addPendingRole(String gameId, Role role, Session session) throws IllegalArgumentException {
	for(PendingGame pg : pendingGames) {
	    if(pg.getId().equals(gameId)) {
		pg.addRoleWithSession(role, session);
		return;
	    }
	}
	PendingGame newPG = new PendingGame(gameId);
	newPG.addRoleWithSession(role, session);
	pendingGames.add(newPG);
    }

    /**
     * deletes a game from the set of current games
     */
    void deleteGameById(String id) {
	Game targetGame = getGameById(id);
	if(targetGame != null) {
	    games.remove(targetGame);
	}
    }

    /** deletes a game from the set of games not yet started
     *
     */
    void deletePendingGame(PendingGame pg) {
	pendingGames.remove(pg);
    }

    /**
     * returns the ongoing game with the given `gameId`; returns `null` if `gameId` doesn't match
     */
    private Game getGameById(String gameId) {
	for(Game currentGame : games) {
	    if(currentGame.getId().equals(gameId)) {
		return currentGame;
	    }
	}
	return null;
    }

    /**
     * returns the set of all ongoing games
     */
    Set<Game> getGames() {
	return games;
    }

    /**
     * returns the set of all players in a game not yet started; returns `null` if no match for `gameId` is found 
     */
    Set<Role> getPendingRoles(String gameId) {
	for(PendingGame pg : pendingGames) {
	    if(pg.getId().equals(gameId)) {
		return pg.getRoles();
	    }
	}
	return null;
    }

    /**
     * returns the set of all sessions in a game not yet started
     */
    Set<Session> getPendingSessions(String gameId) {
	for(PendingGame pg : pendingGames) {
	    if(pg.getId().equals(gameId)) {
		return pg.getSessions();
	    }
	}
	System.out.println("Something is wrong.");
	return new HashSet<Session>(); //should be unreachable in practice
    }

    /**
     * represents a game not yet started
     */
    private class PendingGame {    
	Timer timer;
	List<String> aliasList;
	int gameSize;
	String gameId;
	Map<Role,Session> rolesWithSessions;
	Set<GameServer> endpoints;

	PendingGame(String gameId) {
	    timer = new Timer();
	    aliasList = new ArrayList<String>();
	    aliasList.add("qwe");
	    aliasList.add("asd");
	    aliasList.add("zxc");
	    aliasList.add("rty");
	    aliasList.add("fgh");
	    aliasList.add("vbn");
	    this.gameSize = 5;
	    this.gameId = gameId;
	    rolesWithSessions = new HashMap<Role,Session>();
	    endpoints = new HashSet<GameServer>();
	}

        /**
         * adds a player to the game not yet started
         * @param role the player
         * @param session the session between the player and their GameServer instance
         */
	void addRoleWithSession(Role role, Session session) throws IllegalArgumentException {
	    for(Role currentRole : rolesWithSessions.keySet()) {
		if(currentRole == role) {
		    throw new IllegalArgumentException("Cannot play two games simultaneously.");
		}
	    }
	    rolesWithSessions.put(role, session);
	    if(rolesWithSessions.size() == gameSize) {
		Role[] roles = rolesWithSessions.keySet().toArray(new Role[0]);
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
			    for(Role currentRole : roles) {
				if(currentRole.getAlias().equals("")) {
				    String newAlias;
                                    outerloop: while(true) {
                                        newAlias = PendingGame.this.getRandomAlias();
					for(Role cr : roles) {
					    if(cr.getAlias().equals(newAlias)) {
						continue outerloop;
					    }
					}
					currentRole.setAlias(newAlias);
					break;
				    }
				}
			    }
			}
		    }, 10*1000);
		try {
		    Game game = new Game(gameId, roles, rolesWithSessions);
		    games.add(game);
		    GameConfigurator.this.deletePendingGame(this);
		    for(GameServer gs : endpoints) {
			gs.setGame(game);
		    }
		    game.start();
		    try {
			game.t.join();
		        games.remove(game);
                    } catch(InterruptedException e) {
			e.printStackTrace();
		    }
		} catch(IllegalArgumentException e) {
		    System.out.println("IllegalArgumentException: " + e.getMessage());
		    e.printStackTrace();
		}
	    }
	}
        
	void addEndpoint(GameServer endpoint) {
	    endpoints.add(endpoint);
	}
        
	String getId() {
	    return gameId;
	}
        
	Set<Role> getRoles() {
	    return rolesWithSessions.keySet();
	}
        
	Set<Session> getSessions() {
	    return new HashSet<Session>(rolesWithSessions.values());
	}

	private String getRandomAlias() {
	    Collections.shuffle(aliasList);
	    return aliasList.get(0);
	}
    }
}
