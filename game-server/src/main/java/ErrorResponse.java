package mafia.game;

enum ErrorResponse {
    ALIAS_TAKEN("alias-taken");

    private final String value;

    private ErrorResponse(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    @Override
    public String toString() {
	return getValue();
    }
}
