package mafia.game;

enum NormalResponse {
    AUTHENTICATED("authenticated"),
    TOKEN_RECEIVED("token-received"),
    ALIAS("alias"),
    FIRST_DAY("first-day"),
    CASUALTIES("casualties"),
    CURRENT_STATE("current-state"),
    CHAT_ENABLED("chat-enabled"),
    TRIAL_OPEN("trial-enabled"),
    TRIAL_VOTE_ADDED("trial-vote-added"),
    TRIAL_VOTE_REMOVED("trial-vote-removed"),
    TRIAL_INTRO("trial-intro"),
    TRIAL_DEFENSE("trial-defense"),
    TRIAL_VOTING_ENABLED("trial-voting-enabled"),
    TRIAL_VOTING_DISABLED("trial-voting-disabled"),
    VERDICT_INNOCENT("verdict-innocent"),
    VERDICT_GUILTY("verdict-guilty"),
    DAY_TRIAL_CLOSED("day-trial-closed"),
    DAY_END("day_end"),
    BEGIN_NIGHT("begin-night"),
    END_NIGHT("end-night"),
    MESSAGE("message"),
    DEAD_MESSAGE("dead-message");
    

    private final String value;

    private NormalResponse(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    @Override
    public String toString() {
	return getValue();
    }
}
