package mafia.game;

import java.util.*;
import javax.websocket.*;
import javax.websocket.server.*;

public class GameServerConfig implements ServerApplicationConfig {
    @Override
    public Set<ServerEndpointConfig> getEndpointConfigs(Set<Class<? extends Endpoint>> endpointClasses) {
	Set<ServerEndpointConfig> configs = new HashSet<ServerEndpointConfig>();
	ServerEndpointConfig sec = ServerEndpointConfig.Builder.create(GameServer.class, "/game").configurator(new GameConfigurator()).build();
	configs.add(sec);
	return configs;
    }
    
    public Set<Class<?>> getAnnotatedEndpointClasses(Set<Class<?>> scanned) {
	return scanned;
    }
}
