package mafia.game;

import java.io.IOException;
import java.util.*;
import javax.websocket.*;
import javax.websocket.server.*;

/**
 * Represents a game
 * @author Ramin Soltanzadeh
 * @version 1.0
 */
class Game implements Runnable {
    Thread t;

    private final String id;
    private int day;
    private Role[] roles;
    private Set<Role> alive;
    private Set<Role> dead;
    private Map<Role,Session> sessions;
    private Stage stage;
    private int jailed;
    private Role onTrial;
    private Set<Role> guiltyVoters;
    private Set<Role> innocentVoters;
    private Set<Casualty> casualties;
    
    public Game(String id, Role[] roles, Map<Role,Session> sessions) {
	t = new Thread(this);

	day = 0;
	this.id = id;
	this.sessions = sessions;
	stage = Stage.ALIAS;
	alive = new HashSet<Role>(Arrays.asList(roles));
	dead = new HashSet<Role>();
	guiltyVoters = new HashSet<Role>();
	innocentVoters = new HashSet<Role>();
	casualties = new HashSet<Casualty>();
	//making sure roles are in correct order
	Role[] temp = new Role[roles.length];
	for(int i = 0; i < roles.length; i++) {
	    temp[roles[i].getNumber()] = roles[i];
	}
	this.roles = temp;
    }

    void start() {
	t.start();
    }

    public void run() {
	try {
	    while(true) {
                day++;
		if(day > 1) {
                    setStage(Stage.CASUALTIES);
		    int duration = 4;
		    for(Casualty c : casualties) {
			duration += (8 + c.getAdditionalDuration());
		    }
		    sendCasualtiesToAll();
		    casualties.clear();
		    t.sleep(duration*1000);
		} else {
		    setStage(Stage.FIRST_DAY);
		    sendStageToAll();
		    t.sleep(10*1000);
		}
		/*
		  Alignment winner = checkForWin();
		  if(winner != null) {
		  sendWinnerToAll(winner);
		  break;
		  }
		*/
		setStage(Stage.DAY_CHAT_ENABLED);
		sendStageToAll();
		wait(5*1000);

		setStage(Stage.DAY_TRIAL_OPEN);
		sendStageToAll();
                wait(30*1000);

                if(onTrial != null) {
                    setStage(Stage.TRIAL_INTRO);
                    sendStageToAll();
                    t.sleep(5*1000);

                    setStage(Stage.TRIAL_DEFENSE);
                    sendStageToAll();
                    wait(10*1000);

                    setStage(Stage.TRIAL_VOTING_ENABLED);
                    sendStageToAll();
                    wait(20*1000);
		
                    setStage(Stage.TRIAL_VOTING_DISABLED);
                    sendStageToAll();
                    wait(4*1000);
		
                    if(guiltyVoters.size() > innocentVoters.size()) {
                        setStage(Stage.VERDICT_GUILTY);
                        sendStageToAll();
                        wait(10*1000);
                    } else {
                        setStage(Stage.VERDICT_INNOCENT);
                        sendStageToAll();
                        wait(4*1000);
                    }
                }
                setStage(Stage.DAY_TRIAL_CLOSED);
                sendStageToAll();
                wait(4*1000);

                setStage(Stage.DAY_END);
                sendStageToAll();
                t.sleep(4*1000);

                setStage(Stage.NIGHT_ACTION_ENABLED);
                sendStageToAll();
                wait(30*1000);

                setStage(Stage.NIGHT_ACTION_DISABLED);
                sendStageToAll();
                handleBodyguards();
                handleDoctors();
                handleSurvivors();
                handleGodfathers();
                t.sleep(4*1000);
     	    }
	} catch(InterruptedException e) {
	    e.printStackTrace();
	}
    }

    synchronized void putOnTrial(Role r) {
	onTrial = r;
	notifyAll();	
    }
    
    private void sendCasualtiesToAll() {
	StringBuilder builder = new StringBuilder();
	builder.append("[");
	String prefix = "";
	for(Casualty c : casualties) {
	    builder.append(prefix);
	    prefix = ",";
	    builder.append(c.toJSON());
	}
	builder.append("]");

	for(Session s : sessions.values()) {
	    try {
		s.getBasicRemote().sendText(NormalResponse.CASUALTIES.getValue() + builder.toString());
	    } catch(IOException e) {
		e.printStackTrace();
	    }
	}
    }
    
    private void sendCurrentStateToAll() {
	StringBuilder b1 = new StringBuilder();
	b1.append("{\"alive\":");
	StringBuilder b2 = new StringBuilder();
	b2.append("[");
	String prefix = "";
	for(Role r : alive) {
	    b2.append(prefix);
	    prefix = ",";
	    b2.append("{\"number\":" + r.getNumber() + ",\"name\":" + r.getRoleName() + "\"}");
	}
	b2.append("],");
	b1.append(b2.toString()).append("\"dead\":");
	b2 = new StringBuilder();
	b2.append("[");
	prefix = "";
	for(Role r : dead) {
	    b2.append(prefix);
	    prefix = ",";
	    b2.append("{\"number\":" + r.getNumber() + ",\"name\":" + r.getRoleName() + "\"}");
	}
	b2.append("]");
	b1.append(b2.toString());
	
	for(Session s : sessions.values()) {
	    try {
		s.getBasicRemote().sendText(NormalResponse.CURRENT_STATE.getValue() + b1.toString());
	    } catch(IOException e) {
		e.printStackTrace();
	    }
	}
    }

    private void sendStageToAll() {
	String response;
	switch(stage) {
	case FIRST_DAY:
	    StringBuilder builder = new StringBuilder();
	    builder.append(NormalResponse.FIRST_DAY.getValue() + "[");
	    String prefix = "";
	    for(Role r : roles) {
		builder.append(prefix);
		prefix = ",";
		builder.append(r.toJSON());
	    }
	    builder.append("]");
	    response = builder.toString();
	    break;
	case DAY_CHAT_ENABLED:
	    response = NormalResponse.CHAT_ENABLED.getValue();
	    break;
	case DAY_TRIAL_OPEN:
	    response = NormalResponse.TRIAL_OPEN.getValue();
	    break;
	case TRIAL_INTRO:
	    response = NormalResponse.TRIAL_INTRO.getValue() + onTrial.toJSON();
	    break;
	case TRIAL_DEFENSE:
	    response = NormalResponse.TRIAL_DEFENSE.getValue();
	    break;
	case TRIAL_VOTING_ENABLED:
	    response = NormalResponse.TRIAL_VOTING_ENABLED.getValue();
	    break;
	case TRIAL_VOTING_DISABLED:
	    response = NormalResponse.TRIAL_VOTING_DISABLED.getValue();
	    break;
	case VERDICT_INNOCENT:
	    response = NormalResponse.VERDICT_INNOCENT.getValue();
	    break;
	case VERDICT_GUILTY:
	    response = NormalResponse.VERDICT_GUILTY.getValue();
	    break;
	case DAY_TRIAL_CLOSED:
	    response = NormalResponse.DAY_TRIAL_CLOSED.getValue();
	    break;
	case DAY_END:
	    response = NormalResponse.DAY_END.getValue();
	    break;
	case NIGHT_ACTION_ENABLED:
	    response = NormalResponse.BEGIN_NIGHT.getValue();
	    break;
	case NIGHT_ACTION_DISABLED:
	    response = NormalResponse.END_NIGHT.getValue();
	    break;
	default:
	    System.out.println("Something is wrong.");
	    return;
	}
	for(Session s : sessions.values()) {
	    try {
		s.getBasicRemote().sendText(response);
	    } catch(IOException e) {
		e.printStackTrace();
	    }
	} 
    }

    private void setStage(Stage s) {
	stage = s;
    }

    String getId() {
	return id;
    }

    //handle commands
    synchronized void handleMessage(Role r, String message) {
	if(!r.isAlive()) {
	    postMessageInGraveyardChat(r.getAlias(), message);
	} else if(stage == Stage.DAY_CHAT_ENABLED || 
		  stage == Stage.DAY_TRIAL_OPEN ||
		  stage == Stage.TRIAL_VOTING_ENABLED) {
	    if(r.isAlive()) {
		postMessageInPublicChat(r.getAlias(), message);
	    }
	} else if(r == onTrial && stage == Stage.TRIAL_DEFENSE) {
            postMessageInPublicChat(r.getAlias(), message);
	} else if(stage == Stage.NIGHT_ACTION_ENABLED) {
	    if(r.getNumber() == jailed) {
		postMessageInJailChat(r.getAlias(), message);
	    } else if(r instanceof Jailor) {
		postMessageInJailChat("Jailor", message);
	    } else if(r.getAlignment() == Alignment.MAFIA) {
		postMessageInMafiaChat(r.getAlias(), message);
	    }
	}
    }
    
    synchronized void handleTrialVoteAdd(Role r, int number) {
        Role target = roles[number-1];
	if(stage == Stage.DAY_TRIAL_OPEN && target.isAlive()) {
	    int previousVote = removeTrialVote(r);
	    if(previousVote != -1) {
		for(Session s : sessions.values()) {
		    try {
			s.getBasicRemote().sendText(NormalResponse.TRIAL_VOTE_REMOVED.getValue() + r.getAlias() + ':' + previousVote);
		    } catch(IOException e) {
			e.printStackTrace();
		    }
		}
	    }
	    if(target.addTrialVoter(r)) {
		for(Session s : sessions.values()) {
		    try {
			s.getBasicRemote().sendText(NormalResponse.TRIAL_VOTE_ADDED.getValue() + r.getAlias() + ':' + number);
		    } catch(IOException e) {
			e.printStackTrace();
		    }
		}
	    }
	    if(target.getTrialVoters().size() > alive.size() / 2) {
		putOnTrial(target);
		target.resetTrialVoters();
	    }
	}
    }

    synchronized void handleTrialVoteRemove(Role r) {
	if(stage == Stage.DAY_TRIAL_OPEN) {
	    int previousVote = removeTrialVote(r);
	    for(Session s : sessions.values()) {
		try {
		    s.getBasicRemote().sendText(NormalResponse.TRIAL_VOTE_REMOVED.getValue() + r.getAlias() + ':' + previousVote);
		} catch(IOException e) {
		    e.printStackTrace();
		}
	    }	    
	}
    }

    synchronized int removeTrialVote(Role r) {
	for(Role t : roles) {
	    if(t.removeTrialVoter(r)) {
		return t.getNumber();
	    }
	}
	return -1;
    }
   
    synchronized void handleVoteInnocent(Role r) {
	if(stage == Stage.TRIAL_VOTING_ENABLED) {
	    guiltyVoters.remove(r);
	    innocentVoters.add(r);
	}
    }

    synchronized void handleVoteGuilty(Role r) {
	if(stage == Stage.TRIAL_VOTING_ENABLED) {
	    innocentVoters.remove(r);
	    guiltyVoters.add(r);
	}
    }

    synchronized void handleVoteRemove(Role r) {
	if(stage == Stage.TRIAL_VOTING_ENABLED) {
	    innocentVoters.remove(r);
	    guiltyVoters.remove(r);
	}
    }

    synchronized void handleTarget(Role r, int number) {
	r.setTarget(number);
    }

    synchronized void handleSecondTarget(Role r, int number) {
    }

    synchronized void postMessageInPublicChat(String sender, String message) {
	for(Session s : sessions.values()) {
	    try {
		s.getBasicRemote().sendText(NormalResponse.MESSAGE.getValue() + sender + ':' + message);
	    } catch(IOException e) {
		e.printStackTrace();
	    }
	}
    }

    synchronized void postMessageInGraveyardChat(String sender, String message) {
	for(Role r : dead) {
	    try {
		sessions.get(r).getBasicRemote().sendText(NormalResponse.DEAD_MESSAGE.getValue() + sender + ':' + message);
	    } catch(IOException e) {
		e.printStackTrace();
	    }
	}
    }

    synchronized void postMessageInMafiaChat(String sender, String message) {
	for(Role r : roles) {
	    if(r.getAlignment() == Alignment.MAFIA) {
		try {
		    sessions.get(r).getBasicRemote().sendText(NormalResponse.MESSAGE.getValue() + sender + ':' + message);
		} catch(IOException e) {
		    e.printStackTrace();
		}
	    }
	}
    }

    synchronized void postMessageInJailChat(String sender, String message) {
	for(Role r : roles) {
	    if(r instanceof Jailor || r.getNumber() == jailed) {
		try {
		    sessions.get(r).getBasicRemote().sendText(NormalResponse.MESSAGE.getValue() + sender + ':' + message);
		} catch(IOException e) {
		    e.printStackTrace();
		}
	    }
	}
    }

    //handle roles
    private void handleAmnesiacs() {
	for(Role r : roles) {
	    if(r instanceof Amnesiac && r.getTarget() != 0) {
		
	    }
	}
    }

    private void handleArsonists() {
	for(Role r : roles) {
	    if(r instanceof Arsonist) {
		
	    }
	}
    }

    private void handleBlackmailers() {
	for(Role r : roles) {
	    if(r instanceof Blackmailer) {
		
	    }
	}
    }

    private void handleBodyguards() {
	for(Role r : roles) {
	    if(r instanceof Bodyguard) {
		Bodyguard b = (Bodyguard) r;
		if(b.isBlocked()) {
		    //no action
		} else if(b.getTarget() == 0) {
		    //no action
		} else {
		    Role realTarget = roles[b.getTarget()-1];
		    if(b.getTarget() == b.getNumber()) {
			b.useVest();
		    } else if(realTarget.getNumber() == b.getNumber()) {
			//bus driver caused to target self
			//no vest
		    } else {
			realTarget.setProtectedState(true);
			realTarget.setBodyguardNumber(b.getNumber());
		    }
		}
	    }
	}
    }

    private void handleConsiglieres() {
	for(Role r : roles) {
	    if(r instanceof Consigliere) {
		
	    }
	}
    }

    private void handleConsorts() {
	for(Role r : roles) {
	    if(r instanceof Consort && r.getTarget() != 0) {
		Consort c = (Consort) r;
		if(c.getTarget() != jailed) {
		    roles[c.getTarget()-1].setBlockedState(true);
		    if(roles[c.getTarget()-1] instanceof SerialKiller && roles[r.getTarget()-1].isBlocked() == false) {
			roles[c.getTarget()-1].appendCue("You have stabbed someone who blocked you.");
			c.appendCue("You were stabbed by the serial killer you visited.");
			c.setAliveState(false);
			c.setStabbedState(true);
		    } else {
			//the order of these are important
			roles[c.getTarget()-1].appendCue("Someone blocked you.");
			roles[c.getTarget()-1].setBlockedState(true);
		    }
		} else {
		    roles[c.getTarget()-1].appendCue("Someone tried to block you while you were in jail.");
		}
	    }
	}
    }

    private void handleDisguisers() {
	for(Role r : roles) {
	    if(r instanceof Disguiser) {
		
	    }
	}
    }

    private void handleDoctors() {
	for(Role r : roles) {
	    if(r instanceof Doctor) {
		Doctor d = (Doctor) r;
		if(d.isBlocked()) {
		    //no action
		} else if(d.getTarget() == 0) {
		    //no action
		} else if(d.getTarget() == jailed) {
		    d.useHeal();
		} else {
		    d.useHeal();
		    Role target = roles[d.getTarget()-1];
		    target.setDoctorNumber(d.getNumber());
		    target.setHealedState(true);
		}
	    }
	}
    }

    private void handleEscorts() {
	for(Role r : roles) {
	    if(r instanceof Escort && r.getTarget() != 0) {
		Escort e = (Escort) r;
		if(e.getTarget() != jailed) {
		    if(roles[e.getTarget()-1] instanceof SerialKiller && roles[r.getTarget()-1].isBlocked() == false) {
			roles[e.getTarget()-1].appendCue("You have stabbed someone who blocked you.");
			e.appendCue("You were stabbed by the serial killer you visited.");
			e.setAliveState(false);
		        e.setStabbedState(true);

		    } else {
			//the order of these are important
			roles[e.getTarget()-1].appendCue("Someone blocked you.");
			roles[e.getTarget()-1].setBlockedState(true);
		    }
		} else {
		    roles[e.getTarget()-1].appendCue("Someone tried to block you while you were in jail.");
		}
	    }
	}
    }

    private void handleExecutioners() {
	for(Role r : roles) {
	    if(r instanceof Executioner) {
		
	    }
	}
    }

    private void handleFramers() {
	for(Role r : roles) {
	    if(r instanceof Framer) {
		
	    }
	}
    }

    private void handleGodfathers() {
	for(Role r : roles) {
	    if(r instanceof Godfather && r.isAlive()) {
		Godfather g = (Godfather) r;
		if(g.isBlocked()) {
		    //no action
		} else if(g.getTarget() == 0) {
		    //no action
		} else {
		    Role target = roles[g.getTarget() - 1];
		    if(target.isProtected()) {
			g.setAttackedByBodyguardState(true);
			Bodyguard b = (Bodyguard) roles[target.getBodyguardNumber() - 1];
			b.setDuelledState(true);
		    } else if(target.isImmune()) {
			g.appendCue("You target is immune.");
			target.setShotState(true);
		    }
		}
	    }
	}
    }

    private void handleInvestigators() {
	for(Role r : roles) {
	    if(r instanceof Investigator) {
		
	    }
	}
    }

    private void handleJailorsDay() {
	for(Role r : roles) {
	    if(r instanceof Jailor) {	
		Jailor j = (Jailor) r;
		jailed = j.getTarget();
		if(jailed != 0) {
		    r.appendEarlyCue("You have put your target behind bars. You have " + j.getExecutions() + " remaining executions.");
		    roles[jailed].appendEarlyCue("You have been put behind bars.");
		}
	    }
	}
    }

    private void handleJailorsNight() {
	for(Role r : roles) {
	    if(r instanceof Jailor) {
		Jailor j = (Jailor) r;
		if(jailed != 0) {
		    if(j.shouldExecute()) {
			roles[jailed-1].appendCue("You have been executed by the jailor.");
			if(roles[jailed-1].getAlignment() == Alignment.TOWN) {
			    j.removeExecutions();
			    j.appendEarlyCue("The blood of an innocent member of the town is on your hands. You no longer have permission to execute.");
			}
		    } else if(roles[j.getTarget()-1] instanceof SerialKiller) {
			j.setStabbedState(true);
			j.appendCue("You have been attacked by the serial killer you jailed.");
			j.setAliveState(false);
		    }
		} else {
		    //early cue?
		}
	    }
	}
    }

    private void handleJanitors() {
	for(Role r : roles) {
	    if(r instanceof Janitor) {
		
	    }
	}
    }

    private void handleJesters() {
	for(Role r : roles) {
	    if(r instanceof Jester && r.getTarget() != 0) {
		
	    }
	}
    }

    private void handleLookouts() {
	for(Role r : roles) {
	    if(r instanceof Lookout) {
		
	    }
	}
    }

    private void handleMafiosos() {
	for(Role r : roles) {
	    if(r instanceof Mafioso) {
		
	    }
	}
    }

    private void handleSerialKillers() {
	for(Role r : roles) {
	    if(r instanceof SerialKiller && r.getTarget() != 0 && !r.isBlocked()) {
		if(r.getTarget() != jailed) {
		    if(roles[r.getTarget()-1] instanceof Veteran) {
			Veteran v = (Veteran) r;
			if(v.isAlerted()) {
			    return;
			}
		    }
		    if(roles[r.getTarget()-1].isProtected()) {
			r.appendCue("You have been attacked by a bodyguard.");
			r.setAliveState(false);
			roles[r.getTarget()-1].appendCue("Someone tried to kill you, but a bodyguard defended you.");
			for(Role s : roles) {
			    if(s instanceof Bodyguard && s.getTarget() == r.getTarget()) {
				s.appendCue("Someone tried to attack your target but you have taken the hit instead.");
				s.setAliveState(false);
				return; //important so that only one BG dies
			    }
			}
		    } else if(roles[r.getTarget()-1].isHealed()) {
			roles[r.getTarget()-1].appendCue("You have been stabbed by a serial killer.");
			roles[r.getTarget()-1].appendCue("You have been nursed back to health");
			for(Role s : roles) {
			    if(s instanceof Doctor && s.getTarget() == r.getTarget()) {
				s.appendCue("Someone attacked your target but you have nursed them back to health.");
				//not returning here
			    }
			}
		    } else if(roles[r.getTarget()-1].isImmune()) {
			roles[r.getTarget()-1].appendCue("Someone tried to attack you, but you are immune.");
			r.appendCue("Your target was immune to your attack.");
		    } else {
			roles[r.getTarget()-1].appendCue("You have been stabbed by a serial killer.");
			roles[r.getTarget()-1].setAliveState(false);
		    }
		} else {
		    roles[r.getTarget()-1].appendCue("Someone tried to attack you while you were in jail.");
		    r.appendCue("Your target was immune to your attack.");
		}
	    }
	}
    }

    private void handleSpies() {
	for(Role r : roles) {
	    if(r instanceof Spy) {
		
	    }
	}
    }

    private void handleSurvivors() {
	for(Role r : roles) {
	    if(r instanceof Survivor) {
		Survivor s = (Survivor) r;
		if(s.getTarget() == s.getNumber()) {
		    s.useVest();
		}
	    }
	}
    }

    private void handleTransporters() {
	for(Role r : roles) {
	    if(r instanceof Transporter && r.getTarget() != 0) {
		Transporter t = (Transporter) r;
		if(t.getSecondTarget() != 0) {
		    int firstTarget = t.getTarget();
		    int secondTarget = t.getSecondTarget();
		    if(firstTarget == secondTarget) {
			//not possible
		    } else if(firstTarget != jailed && secondTarget != jailed) {
			Role temp = roles[firstTarget-1];
			roles[firstTarget-1] = roles[secondTarget-1];
			roles[secondTarget-1] = temp;
			roles[firstTarget-1].appendCue("You have been transported.");
			roles[secondTarget-1].appendCue("You have been transported.");
		    } else {
			t.appendCue("One of your targets are in jail. Transportation aborted.");
			if(firstTarget == jailed) {
			    roles[firstTarget-1].appendCue("Someone offers you a ride, but you are in jail and cannot accept it.");
			} else {
			    roles[secondTarget-1].appendCue("Someone offers you a ride, but you are in jail and cannot accept it.");
			}
		    }
		}
	    }
	}
    }

    private void handleVeterans() {
	for(Role r : roles) {
	    if(r instanceof Veteran && r.getTarget() != 0) {
		Veteran v = (Veteran) r;
		if(r.getTarget() == r.getNumber()) {
		    v.useAlert();
		    for(int i = 0; i < roles.length; i++) {
			if(roles[roles[i].getTarget()] == v) {
			    roles[i].appendCue("You have been shot by the veteran you visited.");
			    v.appendCue("You shot someone who visited you.");
			    if(roles[i].isHealed()) {
				roles[i].appendCue("Someone nursed you back to health.");
			    } else {
				roles[i].setAliveState(false);
			    }
			}
		    }
		} else {
		    //not possible
		}
	    }
	}
    }

    private void handleVigilantes() {
	for(Role r : roles) {
	    if(r instanceof Vigilante) {
		
	    }
	}
    }

    private void handleWitches() {
	for(Role r : roles) {
	    if(r instanceof Witch) {
		
	    }
	}
    }
    private class Casualty {
	private Role role;
	private String alias;
	private String roleName;
	private String will;
	private String deathNote;
	private int number;
	private int additionalDuration;

	Casualty(Role role) {
	    this.role = role;
	    alias = role.getAlias();
	    roleName = role.getRoleName();
	    number = role.getNumber();
	    if((will = role.getWill()) != null) {
		additionalDuration += 5;
	    }
	    if((deathNote = role.getDeathNote()) != null) {
		additionalDuration += 5;
	    }
	}

	int getAdditionalDuration() {
	    return additionalDuration;
	}
        
	Role getRole() {
	    return role;
	}

	String toJSON() {
	    StringBuilder builder = new StringBuilder();
	    return builder.
		append("{").
		append("\"alias\":").
		append("\"" + alias + "\"").
		append(",\"roleName\":").
		append("\"" + roleName + "\"").
		append(",\"number\":").
		append(number).
		append(",\"will\":").
		append("\"" + will + "\"").
		append(",\"deathNote\":").
		append("\"" + deathNote + "\"").
		append("}").
		toString();
	}
    }
}

abstract class Role {
    protected String roleName;
    protected Set<Role> trialVoters;
    protected boolean[] possibleTargets;
    protected int target;
    protected int number;
    protected String alias;
    protected String accName;
    protected Alignment alignment;
    protected String will;
    protected String deathNote;
    protected int bodyguardNumber;
    protected int doctorNumber;
    protected boolean isAlive;
    protected boolean isImmune;
    protected boolean isStabbed;
    protected boolean isShot;
    protected boolean isAttackedByBodyguard;
    protected boolean isDoused;
    protected boolean isBlocked;
    protected boolean isHealed;
    protected boolean isProtected;
    protected String cue;
    protected String earlyCue;

    public Role(String accName, int number, String alias) {
	this.accName = accName;
	this.number = number;
	this.alias = alias;
	isAlive = true;
	cue = "";
	earlyCue = "";
    }

    String getRoleName() {
	return roleName;
    }

    boolean addTrialVoter(Role r) {
	return trialVoters.add(r);
    }

    boolean removeTrialVoter(Role r) {
	return trialVoters.remove(r);
    }

    Set<Role> getTrialVoters() {
	return trialVoters;
    }

    void resetTrialVoters() {
	trialVoters.clear();
    }

    void setAlias(String alias) {
        this.alias = alias;
    }

    void setNumberOfPlayers(int a) {
	possibleTargets = new boolean[a+1];
    }
    void writeWill(String s) { 
	will = s; 
    }
    void writeNote(String s) { 
	deathNote = s; 
    }
    void setBodyguardNumber(int a) {
	bodyguardNumber = a;
    }
    void setDoctorNumber(int a) {
	doctorNumber = a;
    }
    void setTarget(int a) {
	if(a >= possibleTargets.length - 1 || a < 0) {
	    target = 0;
	} else {
	    if(possibleTargets[a]) {
		target = a;
	    }
	}
    }
    int getTarget() {
	return target;
    }
    String getNote() { 
	return deathNote;
    }
    String getWill() {
	return will; 
    }

    String getDeathNote() {
	return deathNote;
    }

    int getNumber() {
	return number;
    }
    String getAlias() {
	return alias;
    }
    Alignment getAlignment() {
	return alignment; 
    }
    int getBodyguardNumber() {
	return bodyguardNumber;
    }
    int getDoctorNumber() {
	return doctorNumber;
    }
    void setAliveState(boolean b) {
	isAlive = b; 
    }
    void setImmuneState(boolean b) {
	isImmune = b; 
    }
    void setStabbedState(boolean b) {
	isStabbed = b;
    }
    void setShotState(boolean b) {
	isShot = b;
    }
    void setAttackedByBodyguardState(boolean b) {
	isAttackedByBodyguard = b;
    }
    void setDousedState(boolean b) {
	isDoused = b;
    }
    void setBlockedState(boolean b) {
	isBlocked = b;
    }
    void setHealedState(boolean b) {
	isHealed = b;
    }
    void setProtectedState(boolean b) {
	isProtected = b;
    }
    boolean isAlive() {
	return isAlive;
    }
    boolean isImmune() {
	return isImmune;
    }
    boolean isStabbed() {
	return isStabbed;
    }
    boolean isShot() {
	return isShot;
    }
    boolean isAttackedByBodyguard() {
	return isAttackedByBodyguard;
    }
    boolean isDoused() {
	return isDoused;
    }
    boolean isBlocked() {
	return isBlocked;
    }
    boolean isHealed() {
	return isHealed;
    }
    boolean isProtected() {
	return isProtected;
    }
    void setPossibleTarget(int i, boolean b) { 
	if(i < 0 || i >= possibleTargets.length) {
	    return;
	}
	possibleTargets[i] = b;
    }
    boolean[] getPossibleTargets() {
	return possibleTargets;
    }   
    void appendCue(String s) {
	cue = cue + "\n" + s;
    }
    String getCue() {
	return cue;
    }
    void resetCue() {
	cue = "";
    }
    void appendEarlyCue(String s) {
	earlyCue = earlyCue + "\n" + s;
    }
    String getEarlyCue() {
	return earlyCue;
    }
    void resetEarlyCue() {
	earlyCue = "";
    }
    void reset() {
	trialVoters.clear();
	setTarget(0);
	isBlocked = false;
	isHealed = false;
	isProtected = false;
    }

    String toJSON() {
	StringBuilder builder = new StringBuilder();
	return builder.
	    append("{").
	    append("\"roleName\":").
	    append("\"" + roleName + "\"").
	    append(",\"alias\":").
	    append("\"" + alias + "\"").
	    append(",\"number\":").
	    append("\"" + number + "\"").
	    append(number).
	    append("}").
	    toString();
    }
}

//neutrals
class SerialKiller extends Role {
    private String deathMessage;
    
    public SerialKiller(String accName, int number, String alias) {
	super(accName, number, alias);
	alignment = Alignment.NEUTRAL;
	isImmune = true;
	for(int i = 1; i < 16; i++) {
	    if(i != number) {
		setPossibleTarget(i, true);
	    }
	}
    }
    
    void setMessage(String s) {
	deathMessage = s;
    }

    String getMessage() {
	return deathMessage;
    }

    void reset() {
	setTarget(0);
	setBlockedState(false);
    }
}

class Arsonist extends Role {
    private String deathMessage;

    public Arsonist(String accName, int number, String alias) {
	super(accName, number, alias);
	alignment = Alignment.NEUTRAL;
	for(int i = 1; i < 16; i++) {
	    setPossibleTarget(i, true);
	}
    }

    void setMessage(String s) {
	deathMessage = s;
    }

    String getMessage() {
	return deathMessage;
    }

    void reset() {
	setTarget(0);
	setBlockedState(false);
    }
}
/*
  class Vampire extends Role {
  }

  class Mass Murderer extends Role {
  }
*/

class Witch extends Role {
    private int secondTarget;
    
    public Witch(String accName, int number, String alias) {
	super(accName, number, alias);
	alignment = Alignment.NEUTRAL;
	for(int i = 1; i < 16; i++) {
	    setPossibleTarget(i, true);
	}
    }

    void setSecondTarget(int i) {
	secondTarget = i;
    }

    int getSecondTarget(int i) {
	return secondTarget;
    }

    void reset() {
	setTarget(0);
	setSecondTarget(0);
	setBlockedState(false);
    }
}

class Executioner extends Role {
    private final String targetAcc;

    public Executioner(String accName, int number, String alias, String targetAcc) {
	super(accName, number, alias);
	alignment = Alignment.NEUTRAL;
	this.targetAcc = targetAcc;
	isImmune = true;
    }

    String getTargetAcc() {
	return targetAcc;
    }

    void reset() {
	setBlockedState(false);
    }
}

class Jester extends Role {
    public Jester(String accName, int number, String alias) {
	super(accName, number, alias);
	alignment = Alignment.NEUTRAL;
    }

    void reset() {
	setTarget(0);
	setBlockedState(false);
    }
}

class Survivor extends Role {
    private int remainingVests;

    public Survivor(String accName, int number, String alias) {
	super(accName, number, alias);
	roleName = "Survivor";
	alignment = Alignment.NEUTRAL;
    }
    
    @Override
    void setNumberOfPlayers(int a) {
	super.setNumberOfPlayers(a);
	setPossibleTarget(number, true);
    }

    void useVest() {
	isImmune = true;
	remainingVests--;
	if(remainingVests == 0) {
	    setPossibleTarget(number, false);
	}
    }

    int getVests() {
	return remainingVests;
    }

    @Override
    void reset() {
	super.reset();
	isImmune = false;
    }
}

class Amnesiac extends Role {

    public Amnesiac(String accName, int number, String alias) {
	super(accName, number, alias);
	alignment = Alignment.NEUTRAL;
    }

    void reset() {
	setTarget(0);
	setBlockedState(false);
    }
}

//townies
class Bodyguard extends Role {
    private int remainingVests;
    private boolean duelled;
    public Bodyguard(String accName, int number, String alias) {
	super(accName, number, alias);
	roleName = "Bodyguard";
	alignment = Alignment.TOWN;
	remainingVests = 1;
	duelled = false;
    }

    @Override
    void setNumberOfPlayers(int a) {
	super.setNumberOfPlayers(a);
	for(int i = 1; i < possibleTargets.length; i++) {
	    setPossibleTarget(i, true);
	}
    }

    void setDuelledState(boolean b) {
	duelled = b;
    }

    void useVest() {
	remainingVests--;
	isImmune = true;
	if(remainingVests == 0) {
	    setPossibleTarget(number, false);
	}
    }
    
    int getVests() {
	return remainingVests;
    }

    @Override
    void reset() {
	super.reset();
	isImmune = false;
    }
}

class Lookout extends Role {
    
    public Lookout(String accName, int number, String alias) {
	super(accName, number, alias);
	alignment = Alignment.TOWN;
	for(int i = 1; i < 16; i++) {
	    if(i != number) {
		setPossibleTarget(i, true);
	    }
	}
    }

    void reset() {
	setTarget(0);
	setBlockedState(false);
    }
}

class Jailor extends Role {
    private String deathMessage;
    private int remainingExecutions;
    private boolean execute;

    public Jailor(String accName, int number, String alias) {
	super(accName, number, alias);
	alignment = Alignment.TOWN;
	execute = false;
	for(int i = 1; i < 16; i++) {
	    if(i != number) {
		setPossibleTarget(i, true);
	    }
	}
    }
    
    void execute() {
	if(getExecutions() > 0) {
	    execute = true;
	    remainingExecutions--;
	}
    }

    void removeExecutions() {
	remainingExecutions = 0;
    }

    int getExecutions() {
	return remainingExecutions;
    }

    void setMessage(String s) {
	deathMessage = s;
    }

    String getMessage() {
	return deathMessage;
    }

    boolean shouldExecute() {
	return execute;
    }

    void reset() {
	setTarget(0);
	execute = false;
	setBlockedState(false);
    }
}

class Doctor extends Role {
    private int remainingHeals;

    public Doctor(String accName, int number, String alias) {
	super(accName, number, alias);
	roleName = "Doctor";
	alignment = Alignment.TOWN;
	remainingHeals = 3;
    }
    
    @Override
    void setNumberOfPlayers(int a) {
	super.setNumberOfPlayers(a);
	for(int i = 1; i < possibleTargets.length; i++) {
	    setPossibleTarget(i, true);
	}
    }
    
    void useHeal() {
	remainingHeals--;
	if(remainingHeals == 0) {
	    for(int i = 1; i < possibleTargets.length; i++) {
		setPossibleTarget(i, false);
	    }
	}
    }

    int getHeals() {
	return remainingHeals;
    }

    @Override
    void reset() {
	super.reset();
	isImmune = false;
    }    
}


class Transporter extends Role {
    private int secondTarget;

    public Transporter(String accName, int number, String alias) {
	super(accName, number, alias);
	alignment = Alignment.TOWN;
	for(int i = 1; i < 16; i++) {
	    setPossibleTarget(i, true);
	}
    }

    void setSecondTarget(int a) {
	secondTarget = a;
    }

    int getSecondTarget() {
	return secondTarget;
    }

    void reset() {
	setTarget(0);
	setSecondTarget(0);
	setBlockedState(false);
    }
}
/*
  class Sheriff extends Role {
  }

  class Retributionist extends Role {
  }
*/

class Investigator extends Role {

    public Investigator(String accName, int number, String alias) {
	super(accName, number, alias);
	alignment = Alignment.TOWN;
	for(int i = 1; i < 16; i++) {
	    if(i != number) {
		setPossibleTarget(i, true);
	    }
	}
    }

    void reset() {
	setTarget(0);
	setBlockedState(false);
    }
}

class Spy extends Role {

    public Spy(String accName, int number, String alias) {
	super(accName, number, alias);
	alignment = Alignment.TOWN;
    }

    void reset() {
	setBlockedState(false);
    }
}

class Escort extends Role {

    public Escort(String accName, int number, String alias) {
	super(accName, number, alias);
	alignment = Alignment.TOWN;
    }
    
    void reset() {
	setTarget(0);
	setBlockedState(false);
    }
}

/*
  class Medium extends Role {
  }

  class Mayor extends Role {
  }

  class VampireHunter extends Role {
  }
*/

class Veteran extends Role {
    private String deathMessage;
    private int remainingAlerts;
    private boolean isAlerted;

    public Veteran(String accName, int number, String alias) {
	super(accName, number, alias);
	alignment = Alignment.TOWN;
	setPossibleTarget(number, true);
	isAlerted = false;
    }
    
    void useAlert() {
	isAlerted = true;
	remainingAlerts--;
	if(remainingAlerts == 0) {
	    setPossibleTarget(number, false);
	}
    }

    int getAlerts() {
	return remainingAlerts;
    }

    boolean isAlerted() {
	return isAlerted;
    }

    void setMessage(String s) {
	deathMessage = s;
    }

    String getMessage() {
	return deathMessage;
    }

    void reset() {
	setTarget(0);
	isAlerted = false;
	setBlockedState(false);
    }
}

class Vigilante extends Role {
    private String deathMessage;
    private int remainingShots;

    public Vigilante(String accName, int number, String alias) {
	super(accName, number, alias);
	alignment = Alignment.TOWN;
	for(int i = 1; i < 16; i++) {
	    if(i != number) {
		setPossibleTarget(i, true);
	    }
	}
    }

    void setMessage(String s) {
	deathMessage = s;
    }

    String getMessage() {
	return deathMessage;
    }

    void reset() {
	setTarget(0);
	setBlockedState(false);
    }
    
}


//mafias
class Godfather extends Role {
    private String deathMessage;
    //private int[] mafias;

    public Godfather(String accName, int number, String alias) {
	super(accName, number, alias);
	roleName = "Godfather";
	alignment = Alignment.MAFIA;
    }

    @Override
    void setNumberOfPlayers(int a) {
	super.setNumberOfPlayers(a);
	outerloop:
	for(int i = 1; i < possibleTargets.length; i++) {
	    /*
	      for(int j = 0; j < mafias.length; j++) {
	      if(mafias[j] == i) {
	      continue outerloop;
	      }
	      }
	    */
	    setPossibleTarget(i, true);
	}
    }

    void setMessage(String s) {
	deathMessage = s;
    }

    String getMessage() {
	return deathMessage;
    }

    @Override
    void reset() {
	super.reset();
    }
}


class Mafioso extends Role {
    private String deathMessage;
    private int[] mafias;

    public Mafioso(String accName, int number, String alias, int[] mafias) {
	super(accName, number, alias);
	alignment = Alignment.MAFIA;
	outerloop:
	for(int i = 1; i < 16; i++) {
	    for(int j = 0; j < mafias.length; j++) {
		if(mafias[j] == i) {
		    continue outerloop;
		}
	    }
	    setPossibleTarget(i, true);
	}
    }

    void setMessage(String s) {
	deathMessage = s;
    }

    String getMessage() {
	return deathMessage;
    }

    void reset() {
	setTarget(0);
	setBlockedState(false);
    }
}


class Consigliere extends Role {
    private int[] mafias;

    public Consigliere(String accName, int number, String alias, int[] mafias) {
	super(accName, number, alias);
	alignment = Alignment.MAFIA;
	outerloop:
	for(int i = 1; i < 16; i++) {
	    for(int j = 0; j < mafias.length; j++) {
		if(mafias[j] == i) {
		    continue outerloop;
		}
	    }
	    setPossibleTarget(i, true);
	}
    }

    void reset() {
	setTarget(0);
	setBlockedState(false);
    }
}

class Blackmailer extends Role {
    private int[] mafias;

    public Blackmailer(String accName, int number, String alias, int[] mafias) {
	super(accName, number, alias);
	alignment = Alignment.MAFIA;
	outerloop:
	for(int i = 1; i < 16; i++) {
	    for(int j = 0; j < mafias.length; j++) {
		if(mafias[j] == i) {
		    continue outerloop;
		}
	    }
	    setPossibleTarget(i, true);
	}
    }

    void reset() {
	setTarget(0);
	setBlockedState(false);
    }
}

class Consort extends Role {
    private int[] mafias;

    public Consort(String accName, int number, String alias, int[] mafias) {
	super(accName, number, alias);
	alignment = Alignment.MAFIA;
	outerloop:
	for(int i = 1; i < 16; i++) {
	    for(int j = 0; j < mafias.length; j++) {
		if(mafias[j] == i) {
		    continue outerloop;
		}
	    }
	    setPossibleTarget(i, true);
	}
    }

    void reset() {
	setTarget(0);
	setBlockedState(false);
    }
}

class Janitor extends Role {
    private int remainingCleanings;
    private int[] mafias;

    public Janitor(String accName, int number, String alias, int[] mafias) {
	super(accName, number, alias);
	alignment = Alignment.MAFIA;
	outerloop:
	for(int i = 1; i < 16; i++) {
	    for(int j = 0; j < mafias.length; j++) {
		if(mafias[j] == i) {
		    continue outerloop;
		}
	    }
	    setPossibleTarget(i, true);
	}
    }

    int getCleans() {
	return remainingCleanings;
    }

    void reset() {
	setTarget(0);
	setBlockedState(false);
    }
}

class Disguiser extends Role {
    private int remainingDisguises;
    private int[] mafias;

    public Disguiser(String accName, int number, String alias, int[] mafias) {
	super(accName, number, alias);
	alignment = Alignment.MAFIA;
	outerloop:
	for(int i = 1; i < 16; i++) {
	    for(int j = 0; j < mafias.length; j++) {
		if(mafias[j] == i) {
		    continue outerloop;
		}
	    }
	    setPossibleTarget(i, true);
	}
    }

    int getDisguises() {
	return remainingDisguises;
    }

    void reset() {
	setTarget(0);
	setBlockedState(false);
    }
}

class Framer extends Role {
    private int[] mafias;

    public Framer(String accName, int number, String alias, int[] mafias) {
	super(accName, number, alias);
	alignment = Alignment.MAFIA;
	outerloop:
	for(int i = 1; i < 16; i++) {
	    for(int j = 0; j < mafias.length; j++) {
		if(mafias[j] == i) {
		    continue outerloop;
		}
	    }
	    setPossibleTarget(i, true);
	}
    }

    void reset() {
	setTarget(0);
	setBlockedState(false);
    }
}
/*
  class Forger extends Role {
  }

  //new roles
  class Detective extends Role {
  }

  class Marshall extends Role {
  }

  class Judge extends Role {
  }

  class Crier extends Role {
  }
*/

enum Alignment {
    TOWN, MAFIA, NEUTRAL
}

enum Stage {
    ALIAS,
    FIRST_DAY,
    CASUALTIES,
    DAY_CHAT_ENABLED,
    DAY_TRIAL_OPEN,
    TRIAL_INTRO,
    TRIAL_DEFENSE, 
    TRIAL_VOTING_ENABLED,
    TRIAL_VOTING_DISABLED,
    VERDICT_INNOCENT,
    VERDICT_GUILTY,
    DAY_TRIAL_CLOSED,
    DAY_END,
    NIGHT_ACTION_ENABLED,
    NIGHT_ACTION_DISABLED
}
