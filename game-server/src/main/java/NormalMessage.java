package mafia.game;

enum NormalMessage {
    AUTHENTICATOR("authenticator"),
    GAME_TOKEN("game-token"),
    ALIAS("alias"),
    MESSAGE("message"),
    TRIAL_VOTE_ADD("trial-vote-add"),
    TRIAL_VOTE_REMOVE("trial-vote-remove"),
    VOTE_INNOCENT("vote-innocent"),
    VOTE_GUILTY("vote-guilty"),
    VOTE_REMOVE("vote-remove"),
    TARGET("target"),
    SECOND_TARGET("second-target"),
    REMOVE_TARGET("remove-target"),
    REMOVE_SECOND_TARGET("remove-second-target");
    
    private final String value;

    private NormalMessage(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    @Override
    public String toString() {
	return getValue();
    }
}
