package mafia.chat;

import mafia.User;
import java.sql.*;
import javax.sql.*;
import java.io.IOException;
import java.util.*;
import javax.websocket.*;
import javax.websocket.server.*;

/**
 * Handles direct messages between users. Each user is connected to their own ChatServer instance.
 *
 * @author Ramin Soltanzadeh
 * @version 1.0
 */
public class ChatServer extends Endpoint {
    private Session session;
    private ServerEndpointConfig endpointConfig;
    private ChatConfigurator chatConfig;
    private String userName;
    private boolean authenticated;
    private Set<String> neglectors;
    private DataSource dataSource;

    @Override
    public void onOpen(Session session, EndpointConfig endpointConfig) {
	this.session = session;
	this.endpointConfig = (ServerEndpointConfig) endpointConfig;
	chatConfig = (ChatConfigurator) this.endpointConfig.getConfigurator();
	this.authenticated = false;
	try {
	    dataSource = (DataSource) new javax.naming.InitialContext().lookup("java:/MySqlDS");
	} catch(javax.naming.NamingException e) {
	    System.out.println("NamingException: " + e.getMessage());
	    e.printStackTrace();
	}
	this.session.addMessageHandler(new MessageHandler.Whole<String>() {
		ChatConfigurator chatConfig = ChatServer.this.chatConfig;   
		Session session = ChatServer.this.session;

		@Override
		public void onMessage(String text) {
		    if(!ChatServer.this.authenticated && !text.startsWith(NormalMessage.AUTHENTICATOR.getValue())) {
			System.out.println("No authenticator provided. Closing connection.");
			try {
			    session.close(new CloseReason(new CloseReason.CloseCode() {
				@Override
				public int getCode() {
				    return 1008;
				}
			    }, "No authenticator provided."));
			} catch(IOException e) {
			    System.out.println("IOException: " + e.getMessage());
			    e.printStackTrace();
			} 
		    } else if(text.startsWith(NormalMessage.MESSAGE.getValue())) {
			String recipient = text.substring(NormalMessage.MESSAGE.getValue().length(), text.indexOf(':'));
			String message = text.substring(text.indexOf(':') + 1);
			boolean recipientIsOnline = false;

			for(String onlineUser : chatConfig.getOnlineUserNames()) {
			    if(onlineUser.equalsIgnoreCase(recipient)) {
				recipientIsOnline = true;
			    }
			}
			if(!recipientIsOnline) {
			    try {
				System.out.println(userName + " unsuccessfully tried to send a message to offline user '" + recipient + "'.");
				session.getBasicRemote().sendText(ErrorResponse.OFFLINE + recipient);
			    } catch(IOException e) {
				System.out.println("IOException: " + e.getMessage());
				e.printStackTrace();
				} 
			} else {			
			    for(String neglector : chatConfig.getNeglectors(userName)) {
				if(recipient.equalsIgnoreCase(neglector)) {
				    try {
					System.out.println(userName + " unsuccessfully tried to send a message to neglector '" + recipient + "'.");
					session.getBasicRemote().sendText(ErrorResponse.BLOCKED + recipient);
					return;
				    } catch(IOException e) {
					System.out.println("IOException: " + e.getMessage());
					e.printStackTrace();
				    } 
				}
			    }
			    Session recipientSession = null;
			    for(User user : chatConfig.getOnlineUsers()) {
				if(user.getName().equalsIgnoreCase(recipient)) {
				    for(Session otherSession : session.getOpenSessions()) {
					if(user.getId() == otherSession.getId()) {
					    recipientSession = otherSession;
					}
				    }
				    if(recipientSession == null) {
					System.out.println("Something went wrong.");
					return;
				    }
				}
			    }
			    try {
				recipientSession.getBasicRemote().sendText(NormalResponse.MESSAGE + getUserName() + ":" + message);
				session.getBasicRemote().sendText(NormalResponse.OWN_MESSAGE + recipient + ":" + message);
				System.out.println(userName + " successfully sent a message to himself and '" + recipient + "'.");
			    }
			    catch(IOException e) {
				System.out.println("IOException: " + e.getMessage());
				e.printStackTrace();
			    }
			} 
		    } else if(text.startsWith(NormalMessage.AUTHENTICATOR.getValue())) {
			String name = text.substring(NormalMessage.AUTHENTICATOR.getValue().length(), text.indexOf(':'));
			String pass = text.substring(text.indexOf(':') + 1);
			
			try(Connection conn = ChatServer.this.dataSource.getConnection("ramin", "password");
			    Statement stmt = conn.createStatement();
			    ResultSet rs = stmt.executeQuery("SELECT password FROM mafia.users WHERE username = '" + name + "';")) {
				rs.next();
				if(rs.getString("password").equals(pass)) {
				    ChatServer.this.userName = name;
				    try {
					ChatServer.this.chatConfig.addUser(new User(session.getId(), name));
				    } catch(IllegalArgumentException e) {
					System.out.println("IllegalArgumentException: " + e.getMessage());
					e.printStackTrace();
				    }
				    setNeglectors();
				    session.getBasicRemote().sendText(NormalResponse.AUTHENTICATED.getValue());
				    ChatServer.this.authenticated = true;
				} else {
				    session.close(new CloseReason(new CloseReason.CloseCode() {
					    @Override
					    public int getCode() {
						return 1000;
					    }
					}, "Invalid authenticator."));
				}
			    } catch(SQLException e) {
			    System.out.println("SQLException: " + e.getMessage());
			    e.printStackTrace();
			} catch(IOException e) {
			    System.out.println("IOException: " + e.getMessage());
			    e.printStackTrace();
			}
		    } else if(text.startsWith(NormalMessage.ADD_FRIEND.getValue())) {
			String friendName = text.substring(NormalMessage.ADD_FRIEND.getValue().length());

			String queryFoes = 
			    "SELECT foe_users.username " +
			    "FROM mafia.users AS foe_users " + 
			    "INNER JOIN mafia.foes ON foe_users.id = foes.foe_id " + 
			    "INNER JOIN mafia.users AS this_user ON this_user.id = foes.user_id " + 
			    "WHERE this_user.username = '" + userName + "';";
			String queryFriends = 
			    "SELECT friend_users.username " + 
			    "FROM mafia.users AS friend_users " + 
			    "INNER JOIN mafia.friends ON friend_users.id = friends.friend_id " + 
			    "INNER JOIN mafia.users AS this_user ON this_user.id = friends.user_id " + 
			    "WHERE this_user.username = '" + userName + "';";

			String queryUser =
			    "SELECT id " + 
			    "FROM mafia.users " + 
			    "WHERE username = '" + friendName + "';";

			String updateDeleteFoe = 
			    "DELETE FROM mafia.foes " + 
			    "WHERE user_id = " + 
			    "(" + 
			    "SELECT id " + 
			    "FROM mafia.users " + 
			    "WHERE username = '" + userName + 
			    "') " + 
			    "AND foe_id = " +
			    "(" + 
			    "SELECT id " + 
			    "FROM mafia.users " + 
			    "WHERE username = '" + friendName + 
			    "');";
			String updateAddFriend = 
			    "INSERT INTO " + 
			    "mafia.friends (user_id, friend_id) " + 
			    "VALUES (" + 
			    "(" + 
			    "SELECT id " + 
			    "FROM mafia.users " + 
			    "WHERE username = '" + userName + 
			    "'), " + 
			    "(" + 
			    "SELECT id " + 
			    "FROM mafia.users " +
			    "WHERE username = '" + friendName + 
			    "')" + 
			    ");";

			try(Connection conn = ChatServer.this.dataSource.getConnection("ramin", "password");
			    Statement stmtFoes = conn.createStatement();
			    Statement stmtFriends = conn.createStatement();
			    Statement stmtUser = conn.createStatement();
			    Statement stmtDeleteFoe = conn.createStatement();
			    Statement stmtAddFriend = conn.createStatement();
			    ResultSet rsFoes = stmtFoes.executeQuery(queryFoes);
			    ResultSet rsFriends = stmtFriends.executeQuery(queryFriends);
			    ResultSet rsUser = stmtUser.executeQuery(queryUser)) {
				
				if(friendName.equalsIgnoreCase(userName)) {
				    System.out.println(userName + " unsuccessfully tried to add themself as friend.");
				    session.getBasicRemote().sendText(ErrorResponse.USER_SELF.getValue());
				    return;
				}
				while(rsFoes.next()) {
				    if(rsFoes.getString("username").equalsIgnoreCase(friendName)) {
					stmtDeleteFoe.executeUpdate(updateDeleteFoe);
					chatConfig.removeNeglector(friendName, userName);
					System.out.println(userName + " implicitly deleted '" + friendName + "' as foe by adding them as friend.");
					break;
				    }
				}
				while(rsFriends.next()) {
				    if(rsFriends.getString("username").equalsIgnoreCase(friendName)) {
					System.out.println(userName + " unsuccessfully tried to add '" + friendName + "' as duplicate friend.");
					session.getBasicRemote().sendText(ErrorResponse.ALREADY_FRIEND + friendName);
					return;
				    }
				}
				if(rsUser.next()) {
				    stmtAddFriend.executeUpdate(updateAddFriend);    
				    System.out.println(userName + " successfully added '" + friendName + "' as friend.");
				    session.getBasicRemote().sendText(NormalResponse.FRIEND_ADDED.getValue() + friendName);
				} else {
				    System.out.println(userName + " unsuccessfully tried to add nonexisting user '" + friendName + "' as friend.");
				    session.getBasicRemote().sendText(ErrorResponse.USER_NONEXISTENT + friendName);
				}
			    } catch(SQLException e) {
			    System.out.println("SQLException: " + e.getMessage());
			    e.printStackTrace();
			} catch(IOException e) {
			    System.out.println("IOException: " + e.getMessage());
			    e.printStackTrace();
			}
		    } else if(text.startsWith(NormalMessage.ADD_FOE.getValue())) {
			String foeName = text.substring(NormalMessage.ADD_FOE.getValue().length());

			String queryFoes = 
			    "SELECT foe_users.username " +
			    "FROM mafia.users AS foe_users " +
			    "INNER JOIN mafia.foes ON foe_users.id = foes.foe_id " + 
			    "INNER JOIN mafia.users AS this_user ON this_user.id = foes.user_id " + 
			    "WHERE this_user.username = '" + userName + "';";
			String queryFriends = 
			    "SELECT friend_users.username " +
			    "FROM mafia.users AS friend_users " + 
			    "INNER JOIN mafia.friends ON friend_users.id = friends.friend_id " + 
			    "INNER JOIN mafia.users AS this_user ON this_user.id = friends.user_id " + 
			    "WHERE this_user.username = '" + userName + "';";
			String queryUser = 
			    "SELECT id " + 
			    "FROM mafia.users " + 
			    "WHERE username = '" + foeName + "';";
			String updateDeleteFriend = 
			    "DELETE FROM mafia.friends " +
			    "WHERE user_id = " + 
			    "(" + 
			    "SELECT id " + 
			    "FROM mafia.users " +
			    "WHERE username = '" + userName + 
			    "') " + 
			    "AND friend_id = " +
			    "(" + 
			    "SELECT id " + 
			    "FROM mafia.users " + 
			    "WHERE username = '" + foeName + 
			    "');";
			String updateAddFoe = 
			    "INSERT INTO mafia.foes (user_id, foe_id) " + 
			    "VALUES (" +
			    "(" + 
			    "SELECT id " + 
			    "FROM mafia.users " + 
			    "WHERE username = '" + userName + 
			    "'), " +
			    "(" + 
			    "SELECT id " + 
			    "FROM mafia.users " + 
			    "WHERE username = '" + foeName + 
			    "')" +
			    ");";

			try(Connection conn = ChatServer.this.dataSource.getConnection("ramin", "password");
			    Statement stmtFoes = conn.createStatement();
			    Statement stmtFriends = conn.createStatement();
			    Statement stmtUser = conn.createStatement();
			    Statement stmtDeleteFriend = conn.createStatement();
			    Statement stmtAddFoe = conn.createStatement();
			    ResultSet rsFoes = stmtFoes.executeQuery(queryFoes);
			    ResultSet rsFriends = stmtFriends.executeQuery(queryFriends);
			    ResultSet rsUser = stmtUser.executeQuery(queryUser)) {
				
				if(foeName.equalsIgnoreCase(userName)) {
				    System.out.println(userName + " unsuccessfully tried to add themself as foe.");
				    session.getBasicRemote().sendText(ErrorResponse.USER_SELF.getValue());
				    return;
				}
				while(rsFoes.next()) {
				    if(rsFoes.getString("username").equalsIgnoreCase(foeName)) {
					System.out.println(userName + " unsuccessfully tried to add '" + foeName + "' as duplicate foe.");
					session.getBasicRemote().sendText(ErrorResponse.ALREADY_FOE + foeName);
					return;
				    }
				}
				while(rsFriends.next()) {
				    if(rsFriends.getString("username").equalsIgnoreCase(foeName)) {
					stmtDeleteFriend.executeUpdate(updateDeleteFriend);
					System.out.println(userName + " implicitly deleted '" + foeName + "' as friend by adding them as foe.");
					break;
				    }
				}
				if(rsUser.next()) {
				    stmtAddFoe.executeUpdate(updateAddFoe);
				    chatConfig.addNeglector(foeName, userName);
				    System.out.println(userName + " successfully added '" + foeName + "' as foe.");
				    session.getBasicRemote().sendText(NormalResponse.FOE_ADDED.getValue() + foeName);
				} else {
				    System.out.println(userName + " unsuccessfully tried to add nonexistent user '" + foeName + "' as foe.");
				    session.getBasicRemote().sendText(ErrorResponse.USER_NONEXISTENT + foeName);
				}
			    } catch(SQLException e) {
			    System.out.println("SQLException: " + e.getMessage());
			    e.printStackTrace();
			} catch(IOException e) {
			    System.out.println("IOException: " + e.getMessage());
			    e.printStackTrace();
			}
		    } else if(text.startsWith(NormalMessage.REMOVE_FRIEND.getValue())) {
			String friendName = text.substring(NormalMessage.REMOVE_FRIEND.getValue().length());
			String queryFriends = 
			    "SELECT friend_users.username " +
			    "FROM mafia.users AS friend_users " + 
			    "INNER JOIN mafia.friends ON friend_users.id = friends.friend_id " + 
			    "INNER JOIN mafia.users AS this_user ON this_user.id = friends.user_id " + 
			    "WHERE this_user.username = '" + userName + "';";
			String updateDeleteFriend = 
			    "DELETE FROM mafia.friends " +
			    "WHERE user_id = " + 
			    "(" + 
			    "SELECT id " + 
			    "FROM mafia.users " +
			    "WHERE username = '" + userName + 
			    "') " + 
			    "AND friend_id = " +
			    "(" + 
			    "SELECT id " + 
			    "FROM mafia.users " + 
			    "WHERE username = '" + friendName + 
			    "');";
			try(Connection conn = ChatServer.this.dataSource.getConnection("ramin", "password");
			    Statement stmtFriends = conn.createStatement();
			    Statement stmtDeleteFriend = conn.createStatement();
			    ResultSet rsFriends = stmtFriends.executeQuery(queryFriends)) {
				while(rsFriends.next()) {
				    if(rsFriends.getString("username").equalsIgnoreCase(friendName)) {
					stmtDeleteFriend.executeUpdate(updateDeleteFriend);
					System.out.println(userName + " successfully removed '" + friendName + "' as friend.");
					session.getBasicRemote().sendText(NormalResponse.FRIEND_REMOVED.getValue() + friendName);
					return;
				    } 
				}
				System.out.println(userName + " unsuccessfully tried to remove non-friend '" + friendName + "' as friend.");
				session.getBasicRemote().sendText(ErrorResponse.NOT_FRIEND.getValue() + friendName);
			    } catch(SQLException e) {
			    System.out.println("SQLException: " + e.getMessage());
			    e.printStackTrace();
			} catch(IOException e) {
			    System.out.println("IOException: " + e.getMessage());
			    e.printStackTrace();
			}
		    } else if(text.startsWith(NormalMessage.REMOVE_FOE.getValue())) {
			String foeName = text.substring(NormalMessage.REMOVE_FOE.getValue().length());
			String queryFoes = 
			    "SELECT foe_users.username " +
			    "FROM mafia.users AS foe_users " +
			    "INNER JOIN mafia.foes ON foe_users.id = foes.foe_id " + 
			    "INNER JOIN mafia.users AS this_user ON this_user.id = foes.user_id " + 
			    "WHERE this_user.username = '" + userName + "';";
			String updateDeleteFoe = 
			    "DELETE FROM mafia.foes " +
			    "WHERE user_id = " + 
			    "(" + 
			    "SELECT id " + 
			    "FROM mafia.users " +
			    "WHERE username = '" + userName + 
			    "') " + 
			    "AND foe_id = " +
			    "(" + 
			    "SELECT id " + 
			    "FROM mafia.users " + 
			    "WHERE username = '" + foeName + 
			    "');";
			try(Connection conn = ChatServer.this.dataSource.getConnection("ramin", "password");
			    Statement stmtFoes = conn.createStatement();
			    Statement stmtDeleteFoe = conn.createStatement();
			    ResultSet rsFoes = stmtFoes.executeQuery(queryFoes)) {
				while(rsFoes.next()) {
				    if(rsFoes.getString("username").equalsIgnoreCase(foeName)) {
					stmtDeleteFoe.executeUpdate(updateDeleteFoe);
					chatConfig.removeNeglector(foeName, userName);
					System.out.println(userName + " successfully removed '" + foeName + "' as foe.");
					session.getBasicRemote().sendText(NormalResponse.FOE_REMOVED.getValue() + foeName);
					return;
				    } 
				}
				System.out.println(userName + " unsuccessfully tried to remove non-foe '" + foeName + "' as foe.");
				session.getBasicRemote().sendText(ErrorResponse.NOT_FOE.getValue() + foeName);
			    } catch(SQLException e) {
			    System.out.println("SQLException: " + e.getMessage());
			    e.printStackTrace();
			} catch(IOException e) {
			    System.out.println("IOException: " + e.getMessage());
			    e.printStackTrace();
			}
		    } else if(text.startsWith(NormalMessage.ONLINE_FRIEND_LIST.getValue())) {
			StringBuilder builder = new StringBuilder();
			String prefix = "";
			for(String friend : ChatServer.this.getOnlineFriends()) {
			    builder.append(prefix);
			    prefix = ",";
			    builder.append(friend);
			}
			try {
			    session.getBasicRemote().sendText(NormalResponse.ONLINE_FRIEND_LIST.getValue() + builder.toString());
			    System.out.println(userName + " has requested and received their friendlist.");
			} catch(IOException e) {
			    System.out.println("IOException: " + e.getMessage());
			    e.printStackTrace();
			}
		    } else {
			System.out.println("Received invalid message: '" + text + "'.");
		    }
		}

		public String getUserName() {
		    return ChatServer.this.userName;
		}
	    });
    }
    public void setNeglectors() {
	String query = "SELECT neglectors.username FROM mafia.users AS neglectors INNER JOIN mafia.foes ON neglectors.id = foes.user_id WHERE foes.foe_id = '" + userName + "';";
	Set<String> neg = new HashSet<String>();
	try(Connection conn = ChatServer.this.dataSource.getConnection("ramin", "password");
	    Statement stmt = conn.createStatement();
	    ResultSet rs = stmt.executeQuery(query)) {
		while(rs.next()) {
		    neg.add(rs.getString("username"));
		}
	    } catch(SQLException e) {
	    System.out.println("SQLException: " + e.getMessage());
	    e.printStackTrace();
	}
	chatConfig.createNeglectorSet(userName, neg);
    }

    public Set<String> getOnlineFriends() {
	String queryFriends = 
	    "SELECT friend_users.username " + 
	    "FROM mafia.users AS friend_users " + 
	    "INNER JOIN mafia.friends ON friend_users.id = friends.friend_id " + 
	    "INNER JOIN mafia.users AS this_user ON this_user.id = friends.user_id " + 
	    "WHERE this_user.username = '" + userName + "';";
	Set<String> online = new HashSet<String>();
	try(Connection conn = ChatServer.this.dataSource.getConnection("ramin", "password");
	    Statement stmtFriends = conn.createStatement();
	    ResultSet rsFriends = stmtFriends.executeQuery(queryFriends)) {
		Set<String> allOnlineUserNames = chatConfig.getOnlineUserNames();
		while(rsFriends.next()) {
		    if(allOnlineUserNames.contains(rsFriends.getString("username"))) {
			online.add(rsFriends.getString("username"));
		    }
		}
	    } catch(SQLException e) {
	    System.out.println("SQLException: " + e.getMessage());
	    e.printStackTrace();
	}
	return online;
    }
    
    @Override
    public void onClose(Session session, CloseReason closeReason) {
	try {
	    chatConfig.removeUserByName(userName);
	} catch(IllegalArgumentException e) {
	    System.out.println("IllegalArgumentException: " + e.getMessage());
	    e.printStackTrace();
	}
    }
}
enum ErrorResponse {
    ALREADY_FRIEND("error-already-friend"),
    ALREADY_FOE("error-already-foe"),
    NOT_FOE("error-not-foe"),
    NOT_FRIEND("error-not-friend"),
    OFFLINE("error-offline"),
    BLOCKED("error-blocked"),
    USER_NONEXISTENT("error-user-nonexistent"),
    USER_SELF("error-user-self");

    private final String value;

    private ErrorResponse(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    @Override
    public String toString() {
	return getValue();
    }
}

enum NormalResponse {
    AUTHENTICATED("authenticated"),
    MESSAGE("message"),
    OWN_MESSAGE("own-message"),
    FRIEND_ADDED("friend-added"),
    FRIEND_REMOVED("friend-removed"),
    FOE_ADDED("foe-added"),
    FOE_REMOVED("foe-removed"),
    ONLINE_FRIEND_LIST("online-friend-list");
    private final String value;

    private NormalResponse(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    @Override
    public String toString() {
	return getValue();
    }
}

enum NormalMessage {
    MESSAGE("message"),
    ADD_FOE("add-foe"),
    ADD_FRIEND("add-friend"),
    REMOVE_FOE("remove-foe"),
    REMOVE_FRIEND("remove-friend"),
    AUTHENTICATOR("authenticator"),
    ONLINE_FRIEND_LIST("online-friend-list");
    private final String value;

    private NormalMessage(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    @Override
    public String toString() {
	return getValue();
    }
}
