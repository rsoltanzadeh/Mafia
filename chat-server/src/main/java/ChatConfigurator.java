package mafia.chat;

import mafia.User;
import java.util.*;
import javax.websocket.*;
import javax.websocket.server.*;

class ChatConfigurator extends ServerEndpointConfig.Configurator {
    private Set<NeglectorSet> neglectorSets;
    private Set<User> onlineUsers;

    ChatConfigurator() {
	onlineUsers = new HashSet<User>();
	neglectorSets = new HashSet<NeglectorSet>();
    }

    void addUser(User user) throws IllegalArgumentException {
	for(User currentUser : onlineUsers) {
	    if(user.getName().equalsIgnoreCase(currentUser.getName())) {
		throw new IllegalArgumentException("User is already online.");
	    }
	}
	onlineUsers.add(user);
    }

    void removeUserByName(String userName) {
	for(User currentUser : onlineUsers) {
	    if(currentUser.getName().equalsIgnoreCase(userName)) {
		onlineUsers.remove(currentUser);
		return;
	    }
	}
    }
    
    void removeUserById(String userId) {
	for(User currentUser : onlineUsers) {
	    if(currentUser.getId().equals(userId)) {
		onlineUsers.remove(currentUser);
		return;
	    }
	}
    }
    
    Set<User> getOnlineUsers() {
	return onlineUsers;
    }

    Set<String> getOnlineUserNames() {
	Set<String> userNames = new HashSet<String>();
	for(User user : onlineUsers) {
	    userNames.add(user.getName());
	}
	return userNames;
    }

    Set<String> getOnlineUserIds() {
	Set<String> userIds = new HashSet<String>();
	for(User user : onlineUsers) {
	    userIds.add(user.getId());
	}
	return userIds;
    }

    void addNeglector(String userName, String neglectorName) throws IllegalArgumentException {
	for(NeglectorSet neglectorSet : neglectorSets) {
	    if(neglectorSet.getUserName().equalsIgnoreCase(userName)) {
		neglectorSet.add(neglectorName);
		return;
	    }
	}
	throw new IllegalArgumentException("No neglector-set for user exists.");
    }

    void removeNeglector(String userName, String neglectorName) {
	for(NeglectorSet neglectorSet : neglectorSets) {
	    if(neglectorSet.getUserName().equalsIgnoreCase(userName)) {
		neglectorSet.remove(neglectorName);
		return;
	    }
	}
    }

    void createNeglectorSet(String userName, Set<String> neglectors) {
	removeNeglectorSet(userName);
	neglectorSets.add(new NeglectorSet(userName, neglectors));
    }

    void removeNeglectorSet(String userName) {
	for(NeglectorSet neglectorSet : neglectorSets) {
	    if(neglectorSet.getUserName().equalsIgnoreCase(userName)) {
		neglectorSets.remove(neglectorSet);
		return;
	    }
	}
    }

    Set<String> getNeglectors(String userName) throws IllegalArgumentException {
	for(NeglectorSet neglectorSet : neglectorSets) {
	    if(neglectorSet.getUserName().equalsIgnoreCase(userName)) {
		return neglectorSet.getNeglectors();
	    }
	}
	throw new IllegalArgumentException("Neglector-set for user doesn't exist.");
    }

    private class NeglectorSet {
	private String userName;
	private Set<String> neglectors;

	NeglectorSet(String userName) {
	    this.userName = userName;
	    neglectors = new HashSet<String>();
	}
	
	NeglectorSet(String userName, Set<String> neglectors) {
	    this(userName);
	    this.neglectors = neglectors;
	}

	void add(String userName) throws IllegalArgumentException {
	    for(String neglector : neglectors) {
		if(neglector.equalsIgnoreCase(userName)) {
		    throw new IllegalArgumentException("Already neglector.");
		}
	    }
	    neglectors.add(userName);
	}

	void remove(String userName) {
	    for(String neglector : neglectors) {
		if(neglector.equalsIgnoreCase(userName)) {
		    neglectors.remove(neglector);
		    return;
		}
	    }
	}

	String getUserName() {
	    return userName;
	}
	
	Set<String> getNeglectors() {
	    return neglectors;
	}
    }
}
